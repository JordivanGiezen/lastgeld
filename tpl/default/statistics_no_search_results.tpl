 <div class="row">
    <div class="col-md-12">
        <h2>Statistical Search</h2>
    </div>
    <div class="col-md-12 clearfix_record"></div>
</div>

<div id="content">
    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
        <li class="active"><a href="#searchform" data-toggle="tab">Search form</a></li>
        <li><a href="#help" data-toggle="tab">Help</a></li>
    </ul>
    <div id="my-tab-content" class="tab-content">
        <div class="tab-pane active" id="searchform">
                    <div class="col-md-12 clearfix_record"></div>
					__INSERT_MESSAGE_HERE__
					<hr>
                    <form class="form-horizontal" role="form" action="" method="post">
                            <h4>Shipmaster</h4>
                            <div class="form-group">
                                <label for="lastname" class="col-sm-2 control-label">Last name*</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last name" value="__LASTNAME__">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="lastname_wc" class="col-sm-2 control-label">Wildcard search</label>
                                <div class="col-sm-10">
                                    <input type="checkbox" id="lastname_wc" name="lastname_wc" __LASTNAME_CHECK__>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="firstname" class="col-sm-2 control-label">First name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="firstname" name="firstname" placeholder="First name" value="__FIRSTNAME__">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="firstname_wc" class="col-sm-2 control-label">Wildcard search</label>
                                <div class="col-sm-10">
                                    <input type="checkbox" id="firstname_wc" name="firstname_wc" __FIRSTNAME_CHECK__>
                                </div>
                            </div>
                            <hr>
                            <h4>Location</h4>
                            <div class="form-group">

                    <label for="region" class="col-sm-2 control-label">Region</label>
                    <div class="col-sm-10">
                    <select id="region" name="region" class="selectpicker" selected="Region">
                        <option value="">Region</option>
						<option>Hamburg, Bremen, Kleine Oost</option>
						<option>Norway</option>
						<option>South Baltic: East Prussia to Lubeck</option>
						<option>France - Atlantic Coast</option>
						<option>England and Wales</option>
						<option>Russia around St. Petersburg</option>
						<option>Livland</option>
						<option>Sweden and Finland</option>
						<option>Spain</option>
						<option>Portugal</option>
						<option>Russian Arctic ports (Archangel)</option>
						<option>Italy</option>
						<option>Denmark</option>
						<option>Scotland</option>
						<option>Other European and Asian Ports in Medit.</option>
						<option>Africa outside the Mediterranean</option>
						<option>France - Mediterranean Coast</option>
						<option>Kurland</option>
						<option>African Mediterranean Ports</option>
						<option>Greenland and Davis Strait</option>
						<option>Esthonia</option>
						<option>The Austrian Netherlands</option>
						<option>The Netherlands</option>
						<option>Mediterranean - unspecified</option>
						<option>Holstein</option>
						<option>Ireland</option>
						<option>North America</option>
						<option>Unindentifiable Ports</option>
                    </select>
                    </div>
                </div>  
                <div class="form-group">
                    <label for="country" class="col-sm-2 control-label">Modern Country</label>
                    <div class="col-sm-10">
                    <select id="country" name="country" class="selectpicker">
                        <option selected value="">Modern Country</option>
						<option>Germany</option>
						<option>Norway</option>
						<option>France</option>
						<option>United Kingdom</option>
						<option>Russia</option>
						<option>Poland</option>
						<option>Latvia</option>
						<option>Estonia</option>
						<option>Sweden</option>
						<option>Spain</option>
						<option>Portugal</option>
						<option>Italy</option>
						<option>Denmark</option>
						<option>Turkey</option>
						<option>Østersøen</option>
						<option>Greenland</option>
						<option>Lithuania</option>
						<option>Finland</option>
						<option>Belgium</option>
						<option>Greece</option>
						<option>The Netherlands</option>
						<option>Mediterranean</option>
						<option>Ireland</option>
						<option>Egypt</option>
						<option>Tunisia</option>
						<option>UK Crown Colony</option>
						<option>Unknown</option>
                    </select>
                                </div>
                            </div>
                            <hr>
                            <h4>Timespan</h4>
                            <div class="form-group">
                                <label for="timebegin" class="col-sm-2 control-label">Begin</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="__BEGIN_DATE__" id="dp1" name="begin">
                                </div>                        
                            </div>
                            <div class="form-group">
                                <label for="timeend" class="col-sm-2 control-label">End</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="__END_DATE__" id="dp2" name="end">
                                </div>                        
                            </div>
							<hr>
                            <h4>Attributes</h4>
                            <div class="form-group">
                                <label for="tax" class="col-sm-2 control-label">Tax</label>
                                <div class="col-sm-2">
                                        <select id="taxOperator" name="taxOperator" class="selectpicker">
                                            <option value="="selected>Exact</option>
                                            <option value=">">More than</option>
                                            <option value="<">Less than</option>
                                            <option value="Between">Between</option>
                                        </select>
                                </div>
                                <div class="col-sm-4">  
                                        <input type="text" class="form-control" id="firstfield" placeholder="" name="tax">
                                </div>
                                <div class="col-sm-4" id="taxSecond">
                                        <input type="text" class="form-control" id="taxMax" placeholder="" name="taxMax">
                                </div> 								
                            </div>
                            <div class="form-group">
                                <label for="tonnage" class="col-sm-2 control-label">Tonnage</label>
                                <div class="col-sm-2">
                                        <select id="tonOperator" name="tonOperator" class="selectpicker">
                                            <option value="="selected>Exact</option>
                                            <option value=">">More than</option>
                                            <option value="<">Less than</option>
                                            <option value="Between">Between</option>
                                        </select>
                                </div>
                                <div class="col-sm-4">  
                                        <input type="text" class="form-control" id="firstfield" placeholder="" name="ton">
                                </div>
                                <div class="col-sm-4" id="tonSecond">
                                        <input type="text" class="form-control" id="tonMax" placeholder="" name="tonMax">
                                </div> 								
                            </div> 
                            <div class="form-group">
                                <label for="loads" class="col-sm-2 control-label">Loads</label>
                                <div class="col-sm-2">
                                        <select id="loadsOperator" name="loadsOperator" class="selectpicker">
                                            <option value="="selected>Exact</option>
                                            <option value=">">More than</option>
                                            <option value="<">Less than</option>
                                            <option value="Between">Between</option>
                                        </select>
                                </div>
                                <div class="col-sm-4">  
                                        <input type="text" class="form-control" id="firstfield" placeholder="" name="loads">
                                </div>
                                <div class="col-sm-4" id="loadsSecond">
                                        <input type="text" class="form-control" id="loadsMax" placeholder="" name="loadsMax">
                                </div> 								
                            </div> 
							<hr>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10"> 
                                    <button type="submit" name="submit" class="btn btn-default">Submit search</button>
                                </div>
                            </div> 	     
                        </form>
        </div>
        <div class="tab-pane" id="help">                
                <div class="col-md-12 clearfix_record"></div>
                <div class="col-md-12">
                    <div class="page-header"><h2>Shipmaster</h2></div>
                    <h3>Last name</h3>
                    <p>Enter the last name of a skipper (e.g. Jacob, Johannes). Tick the 'Wildcard search' box to search for near-matches based on the soundex algorithm. For example, 'Jansen' also finds 'Jensen' or 'Janzen'.</p>
                    <h3>First name</h3>
                    <p>Enter the first name of a skipper (e.g. Jacob, Johannes). Tick the 'Wildcard search' box to search for near-matchesbased on the soundex algorithm. For example, 'Jansen' also finds 'Jensen' or 'Janzen'.</p>
                    <div class="page-header"><h2>Location</h2></div>
                    <h3>Region</h3>
                    <p>Enter the name of a region or province (e.g. Zeeland, Ventspils)</p>
                    <h3>Modern Country</h3>
                    <p>Enter the name of a modern country (e.g. Latvia, Germany)</p>
                    <div class="page-header"><h2>Timespan</h2></div>
                    <h3>Begin and End</h3>
                    <p>Enter a date between 01-04-1744 and 31-12-1748 (DD-MM-YYYY)</p>
                </div>
        </div>
    </div>
</div>