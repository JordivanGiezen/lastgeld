<div class="row">
    <div class="col-md-12">
        <h2><a href="__HTTP_ADDRESS__Advanced/" class="btn btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a> Port: __PORT_NAME__</h2>
    </div>
    <div class="col-md-12 clearfix_record"></div>
</div>
            
<div id="content">
    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
        <li class="active"><a href="#details" data-toggle="tab">Details</a></li>
    </ul>
    <div id="my-tab-content" class="tab-content">
        <div class="tab-pane active" id="details">
                <div class="row">
                    <div class="col-md-12 clearfix_record"></div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="lastname" class="control-label">Port name</label>
                            <input type="text" class="form-control" id="lastname" value="__PORT_NAME__">
                        </div>
                        <div class="form-group">
                            <label for="lastname" class="control-label">Region</label>
                            <input type="text" class="form-control" id="lastname" value="__PORT_REGION__">
                        </div>
                        <div class="form-group">
                            <label for="lastname" class="control-label">Modern country</label>
                            <input type="text" class="form-control" id="lastname" value="__PORT_COUNTRY__">
                        </div>
                        <div class="form-group">
                            <label for="lastname" class="control-label">Sound Code</label>
                            <input type="text" class="form-control" id="lastname" value="__PORT_SOUNDCODE__">
                        </div>
                        <div class="form-group">
                            <label for="lastname" class="control-label">Number of ships</label>
                            <input type="text" class="form-control" id="lastname" value="__PORT_SHIPS__">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <iframe class="maps" src="https://www.google.com/maps/embed/v1/view?key=AIzaSyDzBeYMxQV5gmxxBrUsG0NlepMetPs1tJ8&center=-__PORT_LAT__,__PORT_LNG__&zoom=18&maptype=satellite"frameborder="0"></iframe> <!-- Coordinaten in Center rammen -->
                    </div>                    
                </div>
        </div>
    </div>
</div>