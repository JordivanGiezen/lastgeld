<div class="row">
    <div class="col-md-12">
        <div class="well">
            <h1 class="landing_choice">Tax Statistics</h1>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <ol class="breadcrumb">
          <li><a href="__HTTP_ADDRESS__More/">More</a></li>
          <li class="active">Tax Statistics</li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="well statistics_block">
            <h2 class="landing_choice">Total Tax Revenues</h1>  
            <a class="btn btn-info btn-fullwidth" href="__HTTP_ADDRESS__Statistics/Tax/Amount/">See more</a>
        </div>
    </div>
    <div class="col-md-6">
        <div class="well statistics_block">
            <h2 class="landing_choice">Average tax</h1>
            <a class="btn btn-info btn-fullwidth" href="__HTTP_ADDRESS__Statistics/Tax/Amount/">See more</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="well statistics_block">
            <h2 class="landing_choice">Most tax paying shipmasters</h1>                    
            <a class="btn btn-info btn-fullwidth" href="__HTTP_ADDRESS__Statistics/Tax/Shipmaster/">See more</a>
        </div>
    </div>
    <div class="col-md-6">
        <div class="well statistics_block">
            <h2 class="landing_choice">Most tax paying countries</h1>
            <a class="btn btn-info btn-fullwidth" href="__HTTP_ADDRESS__Statistics/Tax/Countries/">See more</a>
        </div>
    </div>
</div>