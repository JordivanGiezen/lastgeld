<script type="text/javascript">
    // Load the Visualization API and the piechart package.
    google.load('visualization', '1', {'packages':['corechart']});
      
    // Set a callback to run when the Google Visualization API is loaded.
    google.setOnLoadCallback(drawChart);
      
    function drawChart() {
      var jsonData = $.ajax({
          url: "ports.php?action=mostportspercountry",
          dataType:"json",
          async: false
          }).responseText;
      console.log(jsonData);
      // Create our data table out of JSON data loaded from server.
      var data = new google.visualization.DataTable(jsonData);

      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
      chart.draw(data, {title:"Amount of ports per (current) country", width: 800, height: 480});
    }
</script>

<div class="row">
    <div class="col-md-12">
        <ol class="breadcrumb">
          <li><a href="more.php">More</a></li>
          <li><a href="statistics_port.php">Port Statistics</li>
          <li class="active">Number of Ports</li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12" id="graph">
        <div id="chart_div"></div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h3>All ports of origin</h3>    
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="result">
            <div id="results">
              __INSERT_TABLE_HERE__
              </table>
            </div>
        </div>
    </div>
</div>