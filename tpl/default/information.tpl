<div class="row">
	<div class="col-md-2">
	</div>
	<div class="col-md-8">
	    <h1>Information about Lastgeld</h1>
	        <p>Lastgeld is a tax which needed to be paid by shipmasters towards the Admiraliteit. The gross capacity of the ship was determinative for the fee which they had to pay. These payments were collected by the Admiraliteit in one of their offices, located at the main ports of the Repulic of the Seven United Netherlands from 1572 until 1836. For more info, read chapter 3.2.1.4 of Dr. G.M. Welling's thesis <a href="http://dissertations.ub.rug.nl/FILES/faculties/arts/1998/g.m.welling/thesis.pdf">'The Prize of Neutrality'</a>.</p>
	        <p>It is possible to search for information about the lastgeld of the shipping between April 1st 1744 and December 31st 1748. At the regional search section, information about ships originating from specific ports can be found. The genealogical research section contains information about the sailing masters. The other research area can be used to find more detailed information about a subject of your choice.</p>
	</div> 
	<div class="col-md-2">
	</div>
</div>