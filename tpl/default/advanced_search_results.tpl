 <div class="row">
    <div class="col-md-12">
        <h2>Advanced Search</h2>
        <div class="btn-group btn-group-title">
            <a href="__HTTP_ADDRESS____THIS_PAGE__" class="btn btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back to search form</a>
            <a href="__HTTP_ADDRESS____DOWNLOAD__" class="btn btn-default">Download</a>
            <a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#103;&#46;&#109;&#46;&#119;&#101;&#108;&#108;&#105;&#110;&#103;&#64;&#114;&#117;&#103;&#46;&#110;&#108;" class="btn btn-default">Report Error</a>
        </div>   
    </div>
    <div class="col-md-12 clearfix_record"></div>
</div>

<div id="content">
        <div class="tab-pane active" id="searchresults">
            <div class="col-md-12 clearfix"></div>
            <div class="row">
                <div class="col-md-12 mapcol">
                    <div id="map-canvas" class="mapcol"></div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="result">
                        <div id="results">
                            __INSERT_TABLE_HERE__
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDHmhz1TT9nJ1RH0OlptIxXDiu8THJ7vWI"></script>
<script>        
    function initialize(table) {
        var infowindow = new google.maps.InfoWindow({
            content: ''
        });
        var bounds = new google.maps.LatLngBounds();
        var start = new google.maps.LatLng(52.373055,4.899722);
        var mapOptions = {
            zoom: 6,
            center: start,
            mapTypeId: google.maps.MapTypeId.TERRAIN
        };
        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        var marker = [];
        var contentString = [];
        var position = [];
        
        for (i in table){
            position[i] = new google.maps.LatLng(table[i].lat, table[i].lng);
    
            contentString[i] = '<div style="min-width: 10em;">'+
                '<b>'+table[i].portName+'</b>'+
                '<p>'+
                'Total departures: ' + table[i].total + '<br />'+
                'Total cargoes: ' + table[i].cargoes + '<br />'+
                'Total tons: ' + table[i].tons + '<br />'+
                'Total tax: ' + table[i].tax + '<br />'+
                '</p>'+
                '</div>';
                
            marker[i] = new google.maps.Marker({
                position: position[i],
                map: map,
                title: table[i].portName,
                html: contentString[i]
            });
            
            bounds.extend(marker[i].position);
            
            google.maps.event.addListener(marker[i], 'click', function() {
                infowindow.setContent(this.html);
                infowindow.open(map,this);                      
            });
        }
        
        map.fitBounds(bounds);
        var listener = google.maps.event.addListener(map, "idle", function () {
            if (map.getZoom() > 6){
                map.setZoom(6);
            }
            google.maps.event.removeListener(listener);
        });
        
    }
    var table = Array(__INSERT_GMAPS_TABLE_HERE__);
    
    google.maps.event.addDomListener(window, 'load', initialize(table));
</script>