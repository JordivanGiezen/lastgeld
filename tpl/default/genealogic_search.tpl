<div class="row">
    <div class="col-md-12">
        <h2>Genealogical Research</h2>
    </div>
    <div class="col-md-12 clearfix_record"></div>
</div>

<div id="content">
    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
        <li class="active"><a href="#searchform" data-toggle="tab">Search form</a></li>
        <li><a href="#help" data-toggle="tab">Help</a></li>
    </ul>
    <div id="my-tab-content" class="tab-content">
        <div class="tab-pane active" id="searchform">
                    <div class="col-md-12 clearfix_record"></div>
                    <form class="form-horizontal" role="form" action="" method="post">
                            <h4>Shipmaster</h4>
                            <div class="form-group">
                                <label for="lastname" class="col-sm-2 control-label">Last name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="lastname_wc" class="col-sm-2 control-label">Wildcard search</label>
                                <div class="col-sm-10">
                                    <input type="checkbox" id="lastname_wc" name="lastname_wc">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="firstname" class="col-sm-2 control-label">First name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="firstname" name="firstname" placeholder="First name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="firstname_wc" class="col-sm-2 control-label">Wildcard search</label>
                                <div class="col-sm-10">
                                    <input type="checkbox" id="firstname_wc" name="firstname_wc">
                                </div>
                            </div>
                            <hr>
                            <h4>Timespan</h4>
                            <div class="form-group">
                                <label for="timebegin" class="col-sm-2 control-label">Begin</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="01-04-1744" id="dp1" name="begin">
                                </div>                        
                            </div>
                            <div class="form-group">
                                <label for="timeend" class="col-sm-2 control-label">End</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="31-12-1748" id="dp2" name="end">
                                </div>                        
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10"> 
                                    <button type="submit" name="submit" class="btn btn-default">Submit search</button>
                                </div>
                            </div>      
                        </form>
        </div>
        <div class="tab-pane" id="help">                
                <div class="col-md-12 clearfix_record"></div>
                <div class="col-md-12">
                    <div class="page-header"><h2>Shipmaster</h2></div>
                    <h3>Last name</h3>
                    <p>Enter the last name of a skipper (e.g. De Jong, Molman). Tick the 'Wildcard search' box to search for near-matches based on the soundex algorithm. For example, 'Jan_en' finds 'Jansen' and 'Janzen'.</p>
                    <h3>First name</h3>
                    <p>Enter the first name of a skipper (e.g. Jacob, Johannes). Tick the 'Wildcard search' box to search for near-matches based on the soundex algorithm. For example, 'J_n' finds 'Jon' and 'Jan'.</p>
                    <div class="page-header"><h2>Location</h2></div>
                    <h3>Region</h3>
                    <p>Pick a region or province from the list (e.g. Zeeland, Ventspils)</p>
                    <h3>Modern Country</h3>
                    <p>Pick a modern country (e.g. Latvia, Germany)</p>
                    <div class="page-header"><h2>Timespan</h2></div>
                    <h3>Begin and End</h3>
                    <p>Enter a date between 01-04-1744 and 31-12-1748 (DD-MM-YYYY)</p>
                </div>
        </div>
    </div>
</div>
