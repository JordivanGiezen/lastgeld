<div class="row">
    <div class="col-md-12">
        <h2><a href="__HTTP_ADDRESS__Advanced/" class="btn btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a> Entry __SHIPMASTER_ID__: __SHIPMASTER_NAME__</h2>
    </div>
    <div class="col-md-12 clearfix_record"></div>
</div>

<div id="content">
    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
        <li class="active"><a href="#details" data-toggle="tab">Details</a></li>
        <li><a href="#map" data-toggle="tab">Map</a></li>
        <li><a href="#scan" data-toggle="tab">Scan</a></li>
        <li><a href="http://dietrich.soundtoll.nl/public/advanced.php" target="_blank">Sonttol Entry</a></li>
    </ul>
    <div id="my-tab-content" class="tab-content">
        <div class="tab-pane active" id="details">
                <div class="row">
                    <div class="col-md-12 clearfix_record"></div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="lastname" class="control-label">Date</label>
                            <input type="text" class="form-control" id="lastname" value="__SHIPMASTER_DATE__">
                        </div>
                        <div class="form-group">
                            <label for="lastname" class="control-label">Last name</label>
                            <input type="text" class="form-control" id="lastname" value="__SHIPMASTER_LASTNAME__">
                        </div>
                        <div class="form-group">
                            <label for="lastname" class="control-label">Standardized last name</label>
                            <input type="text" class="form-control" id="lastname" value="__SHIPMASTER_LASTNAME_STD__">
                        </div>
                        <div class="form-group">
                            <label for="lastname" class="control-label">First name</label>
                            <input type="text" class="form-control" id="lastname" value="__SHIPMASTER_FIRSTNAME__">
                        </div>
                        <div class="form-group">
                            <label for="lastname" class="control-label">Standardized first name</label>
                            <input type="text" class="form-control" id="lastname" value="__SHIPMASTER_FIRSTNAME_STD__">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="lastname" class="control-label">Departure port</label>
                            <input type="text" class="form-control" id="lastname" value="__SHIPMASTER_PORT__">
                        </div>
                        <div class="form-group">
                            <label for="lastname" class="control-label">Sontcode</label>
                            <input type="text" class="form-control" id="lastname" value="__SHIPMASTER_SOUNDCODE__">
                        </div>
                        <div class="form-group">
                            <label for="lastname" class="control-label">Number of cargoes</label>
                            <input type="text" class="form-control" id="lastname" value="__SHIPMASTER_CARGO__">
                        </div>
                        <div class="form-group">
                            <label for="lastname" class="control-label">Tons</label>
                            <input type="text" class="form-control" id="lastname" value="__SHIPMASTER_TON__">
                        </div>
                        <div class="form-group">
                            <label for="lastname" class="control-label">Tax</label>
                            <input type="text" class="form-control" id="lastname" value="__SHIPMASTER_TAX__">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="lastname" class="control-label">Remarks</label>
                            <textarea class="form-control" id="lastname" rows="3">__SHIPMASTER_REMARK__</textarea>  
                        </div>
                    </div>
                </div>
        </div>
        <div class="tab-pane" id="map">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-12 clearfix_record"></div>
                    <iframe class="maps" src="https://www.google.com/maps/embed/v1/view?key=AIzaSyDzBeYMxQV5gmxxBrUsG0NlepMetPs1tJ8&center=__SHIPMASTER_PORT_LAT__,__SHIPMASTER_PORT_LNG__&zoom=18&maptype=satellite"frameborder="0"></iframe>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="scan">
            <div class="col-md-12 clearfix_record"></div>
            <img class="img-responsive intense" src="http://www.let.rug.nl/welling/lastgeld/scans/__SHIPMASTER_SCAN__"/>
        </div>
    </div>
</div>