<div class="row">
    <div class="col-md-12">
        <div class="well">
            <h1 class="landing_choice">Advanced Search</h1>
            <p class="landing_choice">Perform more detailed search queries.</p>
                <a href="__HTTP_ADDRESS__Advanced/" class="btn btn-info btn-lg btn-fullwidth-center" role="button"><span class="fa fa-search"></span> Search</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="well">
            <h1 class="landing_choice">Statistical Search</h1>
            <p class="landing_choice">Get statistics from the Lastgeld data.</p>
                <a href="__HTTP_ADDRESS__Statistics/" class="btn btn-danger btn-lg btn-fullwidth-center" role="button"><span class="fa fa-bar-chart"></span> Search</a>
        </div>
    </div>
</div>