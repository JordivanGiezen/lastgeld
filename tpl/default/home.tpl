<div class="row">
    <div class="col-md-4">
        <div class="well project">
            <div class="title"><h1 class="landing_choice">Genealogical research</h1><p class="landing_choice">Get more information about genealogical trees by searching for shipmasters.</p></div>
            <a href="__HTTP_ADDRESS__Genealogy/" class="btn btn-success btn-lg btn-fullwidth" role="button"><span class="glyphicon glyphicon-user"></span> Continue</a>
        </div>
    </div>
    <div class="col-md-4">
        <div class="well project">
            <div class="title"><h1 class="landing_choice">Regional research</h1>
            <p class="landing_choice">Get more information about ships originating from a certain port, region or country.</p></div>
            <a href="__HTTP_ADDRESS__Regional/" class="btn btn-primary btn-lg btn-fullwidth" role="button"><span class="glyphicon glyphicon glyphicon-map-marker"></span> Continue</a>
        </div>
    </div>
    <div class="col-md-4">
        <div class="well project">
            <div class="title"><h1 class="landing_choice">Other research</h1><p class="landing_choice">Get more detailed information about subjects you choose.</p></div>
            <a href="__HTTP_ADDRESS__More/" class="btn btn-info btn-lg btn-fullwidth" role="button"><span class="glyphicon glyphicon-search"></span> Continue</a>
        </div>
    </div>
</div>
<div class="row well well-lg well-beneath">
    <div class="col-md-12">
        <h2>Information about Lastgeld</h2>
        <p>Lastgeld is a tax which needed to be paid by shipmasters towards the Admiraliteit. The gross capacity of the ship was determinative for the fee which they had to pay. These payments were collected by the Admiraliteit in one of their offices, located at the main ports of the Repulic of the Seven United Netherlands from 1572 until 1836.</p>
        <a href="__HTTP_ADDRESS__Information/" class="btn btn-default">Read more <span class="glyphicon glyphicon-chevron-right"></span></a>
    </div>
</div>