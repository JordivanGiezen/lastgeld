    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>__INSERT_TITLE_HERE__</title>
        <meta name="apple-mobile-web-app-title" content="Lastgeld">
        <link rel="apple-touch-icon" href="__HTTP_ADDRESS__img/apple/touch-icon-iphone.png">
        <link rel="apple-touch-icon" sizes="76x76" href="__HTTP_ADDRESS__img/apple/touch-icon-ipad.png">
        <link rel="apple-touch-icon" sizes="120x120" href="__HTTP_ADDRESS__img/apple/touch-icon-iphone-retina.png">
        <link rel="apple-touch-icon" sizes="152x152" href="__HTTP_ADDRESS__img/apple/touch-icon-ipad-retina.png">
        <meta name="theme-color" content="#337ab7">
        <link rel="shortcut icon" href="__HTTP_ADDRESS__img/favicon.ico">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!-- Bootstrap Style -->
        <link rel="stylesheet" href="__HTTP_ADDRESS__css/bootstrap.min.css">
        <link rel="stylesheet" href="__HTTP_ADDRESS__css/bootstrap-select.min.css">
        <!-- Table sorting -->
        <link rel="stylesheet" href="__HTTP_ADDRESS__css/sortable.css">
        <!-- Custom Style -->
        <link rel="stylesheet" href="__HTTP_ADDRESS__css/app.min.css">
        <link rel="stylesheet" href="__HTTP_ADDRESS__css/jquery.fancybox.css">
        <!-- Google Charts JS -->
        <script type="text/javascript" src="https://www.google.com/jsapi"></script><!-- Die k*tzooi wil niet draaien wanneer de JS aan het eind zit.-->
		  
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
                
    </head>