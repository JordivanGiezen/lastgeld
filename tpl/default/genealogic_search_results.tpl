<div class="row">    
    <div class="col-md-12">
        <div class="page-header">
            <h3>Results for __SEARCH_QUERY__</h3>                    
            <div class="btn-group btn-group-title">
                <a href="__HTTP_ADDRESS____THIS_PAGE__" class="btn btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back to search form</a>
                <a href="__HTTP_ADDRESS____DOWNLOAD__" class="btn btn-default download">Download</a>
                <a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#103;&#46;&#109;&#46;&#119;&#101;&#108;&#108;&#105;&#110;&#103;&#64;&#114;&#117;&#103;&#46;&#110;&#108;" class="btn btn-default">Report Error</a>
            </div>
        </div>               
    </div>           
</div>

<div class="row">
    <div class="col-md-12">
        <div class="result">
            <div id="results">
				__INSERT_TABLE_HERE__
				</table>
            </div>
        </div>
    </div>
</div>
