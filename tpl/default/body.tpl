    <body>
		__INSERT_MENU_HERE__
        <!-- Wrap all page content here -->
        <!-- Ik heb hier Row weggehaald, omdat we hier en daar wat trucjes met rows moeten toepassen -->
        <div class="container">
                    __INSERT_CONTENT_HERE__
        </div>

        <!-- Bootstrap javascript -->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        <script src="js/datepick.min.js"></script>    
        <script src="js/moment.min.js"></script>
        <script src="js/bootstrap-sortable.js"></script>        
        <script src="js/bootstrap-select.min.js"></script> 
        <script src="js/jquery.fancybox.js"></script>
        <script>
        $(document).ready(function() {
            // Set up image viewer
            $(".fancybox").fancybox();
			
              // Set up date pickers
            window.prettyPrint && prettyPrint();
            $('#dp1').datepicker({
                format: 'dd-mm-yyyy',
                todayBtn: 'linked'
                
            });
            
            $('#dp2').datepicker({
                format: 'dd-mm-yyyy',
                todayBtn: 'linked'
            });

            // Set up form dropdown elements
            $('select').selectpicker();

            $("#taxOperator").change(function(){
                if (this.value == 'Between') {
                    $('#taxMax').fadeIn();
                } else {
                    $('#taxMax').fadeOut();
                }
            });
			$("#tonOperator").change(function(){
                if (this.value == 'Between') {
                    $('#tonMax').fadeIn();
                } else {
                    $('#tonMax').fadeOut();
                }
            });
            $("#loadsOperator").change(function(){
                if (this.value == 'Between') {
                    $('#loadsMax').fadeIn();
                } else {
                    $('#loadsMax').fadeOut();
                }
            });
			
			$('#taxMax').hide();
			$('#tonMax').hide();
			$('#loadsMax').hide();
        }); 
        </script>
    </body>
