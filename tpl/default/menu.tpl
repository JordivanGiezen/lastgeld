<!-- Menu -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Navigatie aan/uit</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="__HTTP_ADDRESS__Home/"><img src="img/logo.png" class="img-responsive navbar-brand-image"></a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li><a href="__HTTP_ADDRESS__Genealogy/">Names</a></li>
          <li><a href="__HTTP_ADDRESS__Regional/">Places</a></li>
          <li><a href="__HTTP_ADDRESS__More/">More</a></li>
          <li><a href="http://siegfried.webhosting.rug.nl/~shipping/forum/" target="_blank">Forum</a></li>
          <li><a href="__HTTP_ADDRESS__Information/">Info</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Lastgeld<b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a target="_blank" href="http://siegfried.webhosting.rug.nl/~shipping/">Database-driven Web Technology</a></li>
              <li class="divider"></li>
              <li><a target="_blank" href="http://siegfried.webhosting.rug.nl/~shipping/paalgeld_weu">Paalgeld Western Europe</a></li>
              <li><a target="_blank" href="http://siegfried.webhosting.rug.nl/~shipping/paalgeld_win">Paalgeld West Indies</a></li>
              <li class="divider"></li>
              <li><a target="_blank" href="http://www.rug.nl/">University of Groningen</a></li>
            </ul>
          </li>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>