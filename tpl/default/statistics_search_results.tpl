 <div class="row">
    <div class="col-md-12">
        <h2>Statistical Search</h2>
        <div class="btn-group btn-group-title">
            <a href="__HTTP_ADDRESS____THIS_PAGE__" class="btn btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back to search form</a>
            <a href="__HTTP_ADDRESS____DOWNLOAD__" class="btn btn-default">Download</a>
            <a href="#" class="btn btn-default" data-toggle="modal" data-target="#myModal">Report Error</a>
        </div>   
    </div>
    <div class="col-md-12 clearfix_record"></div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Report an error</h4>
      </div>
      <form class="form-horizontal" role="form" action="regional_results.php" method="post">
      <div class="modal-body">
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="name" name="name" placeholder="Your name">
            </div>
        </div>
        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="email" name="email" placeholder="Your email">
            </div>
        </div>
        <div class="form-group">
            <label for="problem" class="col-sm-2 control-label">Problem</label>
            <div class="col-sm-10">
                <textarea class="form-control" id="problem" rows="3"></textarea>  
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="submit" name="submit">Send</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div id="content">
        <div class="tab-pane active" id="searchresults">
            <div class="col-md-12 clearfix"></div>
            <div class="row">
                <div class="col-md-12 mapcol">
                    <div id="graph-canvas" class="mapcol"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="result">
                        <div id="results">
                            __INSERT_TABLE_HERE__
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDHmhz1TT9nJ1RH0OlptIxXDiu8THJ7vWI"></script>
<script>        
 google.load("visualization", "1.1", {packages:["bar"]});
	google.setOnLoadCallback(drawChart);
	function drawChart() {
		var data = google.visualization.arrayToDataTable([
			__GRAPH_DATA__
			]);

		var options = {
			chart: {
				title: '__GRAPHNAME__',
				subtitle: '',
			}
		};

		var chart = new google.charts.Bar(document.getElementById('graph-canvas'));

		chart.draw(data, options);
	}
</script>