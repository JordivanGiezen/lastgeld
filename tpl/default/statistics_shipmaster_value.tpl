 <script type="text/javascript">
        
        // Load the Visualization API and the piechart package.
        google.load('visualization', '1', {'packages':['corechart']});
          
        // Set a callback to run when the Google Visualization API is loaded.
        google.setOnLoadCallback(draw);
        
        function draw(){
            drawChartTotal();
            drawChart1744();
            drawChart1745();
            drawChart1746();
            drawChart1747();
            drawChart1748();
        }

        function drawChartTotal() {
          var jsonData = $.ajax({
              url: "shipmaster.php?action=valuepershipmaster",
              dataType:"json",
              async: false
              }).responseText;
          console.log(jsonData);
          // Create our data table out of JSON data loaded from server.
          var data = new google.visualization.DataTable(jsonData);

          // Instantiate and draw our chart, passing in some options.
          var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
          chart.draw(data, {title:"Shipmasters that carried the most value in total", width: 800, height: 480});
        }
          
        function drawChart1744() {
          var jsonData = $.ajax({
              url: "shipmaster.php?action=valuepershipmaster&year=1744",
              dataType:"json",
              async: false
              }).responseText;
          console.log(jsonData);
          // Create our data table out of JSON data loaded from server.
          var data = new google.visualization.DataTable(jsonData);

          // Instantiate and draw our chart, passing in some options.
          var chart = new google.visualization.BarChart(document.getElementById('chart_div44'));
          chart.draw(data, {title: "Year 1744", width: 800, height: 480});
        }

        function drawChart1745() {
          var jsonData = $.ajax({
              url: "shipmaster.php?action=valuepershipmaster&year=1745",
              dataType:"json",
              async: false
              }).responseText;
          console.log(jsonData);
          // Create our data table out of JSON data loaded from server.
          var data = new google.visualization.DataTable(jsonData);

          // Instantiate and draw our chart, passing in some options.
          var chart = new google.visualization.BarChart(document.getElementById('chart_div45'));
          chart.draw(data, {title: "Year 1745", width: 800, height: 480});
        }

            function drawChart1746() {
          var jsonData = $.ajax({
              url: "shipmaster.php?action=valuepershipmaster&year=1746",
              dataType:"json",
              async: false
              }).responseText;
          console.log(jsonData);
          // Create our data table out of JSON data loaded from server.
          var data = new google.visualization.DataTable(jsonData);

          // Instantiate and draw our chart, passing in some options.
          var chart = new google.visualization.BarChart(document.getElementById('chart_div46'));
          chart.draw(data, {title: "Year 1746", width: 800, height: 480});
        }

            function drawChart1747() {
          var jsonData = $.ajax({
              url: "shipmaster.php?action=valuepershipmaster&year=1747",
              dataType:"json",
              async: false
              }).responseText;
          console.log(jsonData);
          // Create our data table out of JSON data loaded from server.
          var data = new google.visualization.DataTable(jsonData);

          // Instantiate and draw our chart, passing in some options.
          var chart = new google.visualization.BarChart(document.getElementById('chart_div47'));
          chart.draw(data, {title: "Year 1747", width: 800, height: 480});
        }

        function drawChart1748() {
          var jsonData = $.ajax({
              url: "shipmaster.php?action=valuepershipmaster&year=1748",
              dataType:"json",
              async: false
              }).responseText;
          console.log(jsonData);
          // Create our data table out of JSON data loaded from server.
          var data = new google.visualization.DataTable(jsonData);

          // Instantiate and draw our chart, passing in some options.
          var chart = new google.visualization.BarChart(document.getElementById('chart_div48'));
          chart.draw(data, {title: "Year 1748", width: 800, height: 480});
        }
</script>

<div class="row">
    <div class="col-md-12">
        <ol class="breadcrumb">
          <li><a href="more.php">More</a></li>
          <li><a href="statistics_port.php">Port Statistics</a></li>
          <li class="active">Value</li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12" id="graph">
        <div id="chart_div"></div>
    </div>
</div>

<div id="content">
    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
        <li class="active"><a href="#1744" data-toggle="tab">1744</a></li>
        <li><a href="#1745" data-toggle="tab">1745</a></li>
        <li><a href="#1746" data-toggle="tab">1746</a></li>
        <li><a href="#1747" data-toggle="tab">1747</a></li>
        <li><a href="#1748" data-toggle="tab">1748</a></li>
    </ul>
    <div id="my-tab-content" class="tab-content">
        <div class="tab-pane active" id="1744">
          <div class="col-md-12">
            <div id="chart_div44"></div>
          </div>
        </div>
        <div class="tab-pane" id="1745">
          <div class="col-md-12">
            <div id="chart_div45"></div>
          </div>
        </div>
        <div class="tab-pane" id="1746">
          <div class="col-md-12">
            <div id="chart_div46"></div>
          </div>
        </div>
        <div class="tab-pane" id="1747">
          <div class="col-md-12">
            <div id="chart_div47"></div>
          </div>
        </div>
        <div class="tab-pane" id="1748">
          <div class="col-md-12">
            <div id="chart_div48"></div>
          </div>
        </div>
    </div>
</div>