<div class="row">
    <div class="col-md-12">
        <ol class="breadcrumb">
          <li><a href="more.php">More</a></li>
          <li><a href="statistics_port.php">Port Statistics</a></li>
          <li class="active">Activity</li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="well statistics_block">
            <h1 class="landing_choice">Countries</h1> 
            <br>
            <a class="btn btn-fullwidth btn-success" href="statistics_port_activity_countries.php"><span class="fa fa-globe"></span> See more</a>
        </div>
    </div>
    <div class="col-md-6">
        <div class="well statistics_block">
            <h1 class="landing_choice">Ports</h1>
            <br>
            <a class="btn btn-info btn-fullwidth" href="statistics_port_activity_ports.php"><span class="fa fa-anchor"></span> See more</a>
        </div>
    </div>
</div>