<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Report an error</h4>
      </div>
      <form class="form-horizontal" role="form" action="regional_results.php" method="post">
      <div class="modal-body">
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="name" name="name" placeholder="Your name">
            </div>
        </div>
        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="email" name="email" placeholder="Your email">
            </div>
        </div>
        <div class="form-group">
            <label for="problem" class="col-sm-2 control-label">Problem</label>
            <div class="col-sm-10">
                <textarea class="form-control" id="problem" rows="3"></textarea>  
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="submit" name="submit">Send</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h2>Regional Research</h2>            
            <div class="btn-group btn-group-title">
                <a href="__HTTP_ADDRESS____THIS_PAGE__" class="btn btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back to search form</a>
                <a href="__HTTP_ADDRESS____DOWNLOAD__" class="btn btn-default">Download</a>
                <a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#103;&#46;&#109;&#46;&#119;&#101;&#108;&#108;&#105;&#110;&#103;&#64;&#114;&#117;&#103;&#46;&#110;&#108;" class="btn btn-default">Report Error</a>
            </div>      
            <div class="maps_hint">
                <p class="mapshint">Click on the <img class="symbol" src="/img/ic_place.png"/></span> in the Google Maps for more information about this port.</p>
            </div>
    </div>           
</div>

<div class="row">
	<div class="col-md-12 mapcol">
		<div id="map-canvas" class="mapcol"></div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="result">
            <div id="results">
                __INSERT_TABLE_HERE__
            </div>
        </div>
    </div>
</div>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDHmhz1TT9nJ1RH0OlptIxXDiu8THJ7vWI"></script>
    <script>		
		function initialize(table) {
			var infowindow = new google.maps.InfoWindow({
				content: ''
			});
			var bounds = new google.maps.LatLngBounds();
			var start = new google.maps.LatLng(52.373055,4.899722);
			var mapOptions = {
				zoom: 6,
				center: start,
				mapTypeId: google.maps.MapTypeId.TERRAIN
			};
			var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
			var marker = [];
			var contentString = [];
			var position = [];
			var populationOptions = [];
			var cityCircle = [];
			
			for (i in table){
				position[i] = new google.maps.LatLng(table[i].lat, table[i].lng);
		
				contentString[i] = '<div style="min-width: 10em;">'+
					'<b>'+table[i].portName+'</b>'+
					'<p>'+
					'Total departures: ' + table[i].total + '<br />'+
					'Total cargoes: ' + table[i].cargoes + '<br />'+
					'Total tons: ' + table[i].tons + '<br />'+
					'Total tax: ' + table[i].tax + '<br />'+
					'</p>'+
					'</div>';
					
				marker[i] = new google.maps.Marker({
					position: position[i],
					map: map,
					title: table[i].portName,
					html: contentString[i]
				});
				populationOptions[i] = {
					strokeColor: '#FF0000',
					strokeOpacity: 0.8,
					strokeWeight: 2,
					fillColor: '#FF0000',
					fillOpacity: 0.35,
					map: map,
					center: position[i],
					radius: Math.sqrt(table[i].total/100)*20000
				};
				
				cityCircle[i] = new google.maps.Circle(populationOptions[i]);
				
				bounds.extend(marker[i].position);
				
				google.maps.event.addListener(marker[i], 'click', function() {
					infowindow.setContent(this.html);
					infowindow.open(map,this);						
				});
			}
			
			map.fitBounds(bounds);
			var listener = google.maps.event.addListener(map, "idle", function () {
				if (map.getZoom() > 6){
					map.setZoom(6);
				}
				google.maps.event.removeListener(listener);
			});
			
		}
		var table = Array(__INSERT_GMAPS_TABLE_HERE__);
		
		google.maps.event.addDomListener(window, 'load', initialize(table));
    </script>
