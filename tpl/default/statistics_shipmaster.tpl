<div class="row">
    <div class="col-md-12">
        <ol class="breadcrumb">
          <li><a href="more.php">More</a></li>
          <li class="active">Shipmaster Statistics</li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="well statistics_block">
            <h2 class="landing_choice">Total number of shipmasters</h2>
            <h1 class="landing_choice">__SHIPMASTER_AMOUNT__</h1>
            <a class="btn btn-info btn-fullwidth" href="statistics_shipmaster_amount.php">See more</a>
        </div>
    </div>
    <div class="col-md-6">
            <div class="well statistics_block">
                <h2 class="landing_choice">Average load</h1>
                <h1 class="landing_choice">__SHIPMASTER_LOAD__ tonnes</h2>
                <a class="btn btn-info btn-fullwidth" href="statistics_shipmaster_load.php">See more</a>
            </div>
    </div>
</div>
<div class="row">

    <div class="col-md-6">
        <div class="well statistics_block">
            <h2 class="landing_choice">Most active shipmasters</h2>                    
            <ol class="center">__SHIPMASTER_AMOUNT__</ol>
            <a class="btn btn-info btn-fullwidth" href="statistics_shipmaster_activity.php">See more</a>
        </div>
    </div>
    <div class="col-md-6">
            <div class="well statistics_block">
                <h2 class="landing_choice">Shipmaster with highest value</h1>
                <ol class="center">__SHIPMASTER_VALUE__ Guilders</ol>
                <a class="btn btn-info btn-fullwidth" href="statistics_shipmaster_loadvalue.php">See more</a>
            </div>
    </div>
</div>