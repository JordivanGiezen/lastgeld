<div class="row">
    <div class="col-md-12">
        <h2>Regional Research</h2>
    </div>
    <div class="col-md-12 clearfix_record"></div>
</div>
            
<div id="content">
    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
        <li class="active"><a href="#searchform" data-toggle="tab">Search form</a></li>
        <li><a href="#help" data-toggle="tab">Help</a></li>
    </ul>
    <div id="my-tab-content" class="tab-content">
        <div class="tab-pane active" id="searchform">
                    <div class="col-md-12 clearfix_record"></div>
                    <form class="form-horizontal" role="form" action="" method="post">
                <div class="form-group">
                    <label for="port" class="col-sm-2 control-label">Port</label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" id="port" name="port" placeholder="Port">
                    </div>
                </div>
                <div class="form-group">
                    <label for="region" class="col-sm-2 control-label">Region</label>
                    <div class="col-sm-10">
                    <select id="region" name="region" class="selectpicker" selected="Region">
                        <option selected value="">Region</option>
						<option>Hamburg, Bremen, Kleine Oost</option>
						<option>Norway</option>
						<option>South Baltic: East Prussia to Lubeck</option>
						<option>France - Atlantic Coast</option>
						<option>England and Wales</option>
						<option>Russia around St. Petersburg</option>
						<option>Livland</option>
						<option>Sweden and Finland</option>
						<option>Spain</option>
						<option>Portugal</option>
						<option>Russian Arctic ports (Archangel)</option>
						<option>Italy</option>
						<option>Denmark</option>
						<option>Scotland</option>
						<option>Other European and Asian Ports in Medit.</option>
						<option>Africa outside the Mediterranean</option>
						<option>France - Mediterranean Coast</option>
						<option>Kurland</option>
						<option>African Mediterranean Ports</option>
						<option>Greenland and Davis Strait</option>
						<option>Esthonia</option>
						<option>The Austrian Netherlands</option>
						<option>The Netherlands</option>
						<option>Mediterranean - unspecified</option>
						<option>Holstein</option>
						<option>Ireland</option>
						<option>North America</option>
						<option>Unindentifiable Ports</option>
                    </select>
                    </div>
                </div>  
                <div class="form-group">
                    <label for="country" class="col-sm-2 control-label">Modern Country</label>
                    <div class="col-sm-10">
                    <select id="country" name="country" class="selectpicker">
						<option selected value="">Modern Country</option>
						<option>Germany</option>
						<option>Norway</option>
						<option>France</option>
						<option>United Kingdom</option>
						<option>Russia</option>
						<option>Poland</option>
						<option>Latvia</option>
						<option>Estonia</option>
						<option>Sweden</option>
						<option>Spain</option>
						<option>Portugal</option>
						<option>Italy</option>
						<option>Denmark</option>
						<option>Turkey</option>
						<option>Østersøen</option>
						<option>Greenland</option>
						<option>Lithuania</option>
						<option>Finland</option>
						<option>Belgium</option>
						<option>Greece</option>
						<option>The Netherlands</option>
						<option>Mediterranean</option>
						<option>Ireland</option>
						<option>Egypt</option>
						<option>Tunisia</option>
						<option>UK Crown Colony</option>
						<option>Unknown</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="soundcode" class="col-sm-2 control-label">Sound Code</label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" id="soundcode" name="soundcode" placeholder="Sound code">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10"> 
                        <button type="submit" name="submit" class="btn btn-default">Submit search</button>
                    </div>
                </div>      
            </form>
        </div>
        <div class="tab-pane" id="help">                
                <div class="col-md-12 clearfix_record"></div>
                <div class="col-md-12">
                    <h3>Port</h3>
                    <p>Enter the name of a city (e.g. Hamburg, Stockholm).</p>
                    <h3>Region</h3>
                    <p>Pick a region or province (e.g. Zeeland, Ventspils).</p>
                    <h3>Modern Country</h3>
                    <p>Pick a modern country (e.g. Latvia, Germany).</p>
                    <h3>Sound Code</h3>
                    <p>Enter the code of a port, as used by the Sound Toll project (e.g. 741KLO, 421DAZ).</p>
                </div>
        </div>
    </div>
</div>