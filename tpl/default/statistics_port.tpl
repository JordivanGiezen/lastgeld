<div class="row">
    <div class="col-md-12">
        <div class="well">
            <h1 class="landing_choice">Port Statistics</h1>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <ol class="breadcrumb">
          <li><a href="__HTTP_ADDRESS__More/">More</a></li>
          <li class="active">Port Statistics</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="well statistics_block">
            <h2 class="landing_choice">Most active<br>ports</h1>                    
                <ol class="center">
                    __STAT_PORT_ACTIVE__
                </ol>
            <a class="btn btn-info btn-fullwidth-center" href="__HTTP_ADDRESS__Statistics/Port/Activity/">See more</a>
        </div>
    </div>
    <div class="col-md-6">
        <div class="well statistics_block">
            <h2 class="landing_choice">Most valuable ports</h1>
                <ol class="center">
                    __STAT_PORT_VALUE__
                </ol>
            <a class="btn btn-info btn-fullwidth-center" href="__HTTP_ADDRESS__Statistics/Port/Value/">See more</a>
        </div>
    </div>
</div>