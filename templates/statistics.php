<?php

/**
 * Class ExtendedTemplate
 *
 * An extension on the template class, specifically for the advanced search page.
 */
class ExtendedTemplate extends Template {


	/**
	 * Consturctor for the template.
	 *
	 * @param $view The view that needs to be used for this template
	 * @param string $page The page which is being processed
	 * @param string $template The template to use
     */
	public function __construct($view, $page, $template = "default"){
		parent::__construct($view, $page, $template);
	}

	/**
	 * Creates the page and generates the data that needs to be showed on the page
	 *
	 * @return mixed|string The page to be shown..
     */

	private function createContent(){	
		
		if ($this->view->noSearch() == true){
			$html = file_get_contents('tpl/'.$this->template.'/statistics_search.tpl');
		} else {
			$download = $this->view->getDownloadArguments();
			$args = $this->view->getArguments();
			
			if (isset($args['showall'])){
				$data = $this->view->getGraphView();
			}
			$records = $this->view->getNrRecords();
			//$query = $this->view->getSearchQueryAdvanced(); //is die nog nodig?
			if ($this->view->isDownload()){
				self::downloadSearchResult();
			} else if ($records == 0){ //meegeven waarop gezocht werd?
				$args = $this->view->getArguments();
				$html = file_get_contents('tpl/'.$this->template.'/statistics_no_search_results.tpl');
				$errormessage = "Your search came back empty.";
				$html = $this->replaceConstant($html, '__INSERT_MESSAGE_HERE__', $errormessage);
				$html = $this->replaceConstant($html, '__LASTNAME__', $args['lastname']);
				$html = $this->replaceConstant($html, '__FIRSTNAME__', $args['firstname']);
				$html = $this->replaceConstant($html, '<option>'.$args['region'].'</option>', '<option selected>'.$args['region'].'</option>');
				$html = $this->replaceConstant($html, '<option>'.$args['country'].'</option>', '<option selected>'.$args['country'].'</option>');
				$html = $this->replaceConstant($html, '__BEGIN_DATE__', $args['begin']);
				$html = $this->replaceConstant($html, '__END_DATE__', $args['end']);
				
				//kan dit niet makkelijker ? :(
				if (isset($args['lastname_wc'])){
					$html = $this->replaceConstant($html, '__LASTNAME_CHECK__', 'checked');
				} else {
					$html = $this->replaceConstant($html, '__LASTNAME_CHECK__', '');
				}
				if (isset($args['firstname_wc'])){
					$html = $this->replaceConstant($html, '__FIRSTNAME_CHECK__', 'checked');
				} else {
					$html = $this->replaceConstant($html, '__FIRSTNAME_CHECK__', '');
				}
				
			} else {
								
				$html = file_get_contents('tpl/'.$this->template.'/statistics_search_results.tpl');
				$graphData = $this->view->getParseForGraph();
				$table = "";
				if (isset($args['showall'])){
					//create a table (this should be cleaner, get it from a template or smt.)
					$table = "<table class=\"table sortable table-responsive table-condensed\">";
					$table .= "<thead><tr><th>".implode('</th><th>', array_keys(current($data)))."</th></tr></thead>";
					
					foreach($data as $id => $row){
						//we want our images displayed with fancybox. see above about cleaning this up.
						$row = preg_replace("/(.*\.jpg)/i", "<a class=\"fancybox\" href=\"img/scans/$1\" title=\"\">View</a>", $row);
						$table .= "<tr id=\"".$id."\"><td>".implode('</td><td>', array_values($row))."</td></tr>";
					}
					$table .= "</table>";
					
				}
					
				
				if (empty($args['graphname'])){
					$args['graphname'] = "Graphical Illustration";
				}
				$html = $this->replaceConstant($html, '__DOWNLOAD__', $download);
				$html = $this->replaceConstant($html, '__INSERT_TABLE_HERE__', $table);
				$html = $this->replaceConstant($html, '__GRAPHNAME__', $args['graphname']);
				$html = $this->replaceConstant($html, '__GRAPH_DATA__', $graphData);
			}
		}
		return $html;
	}
	
	public function createBody($header = true, $menu = true){
	
		$html = file_get_contents('tpl/'.$this->template.'/body.tpl');
				
		$content = $this->createContent();
		
		$menu = $this->createMenu();
		
		//$html = $this->replaceConstant($html, '__INSERT_HEADER_HERE__', $header);
		$html = $this->replaceConstant($html, '__INSERT_MENU_HERE__', $menu);
		$html = $this->replaceConstant($html, '__INSERT_CONTENT_HERE__', $content);
		
		$this->body = $html;
		return true;
	}
	
}
?>
