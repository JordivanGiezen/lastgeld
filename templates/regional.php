<?php

class ExtendedTemplate extends Template {
	
	
	public function __construct($view, $page, $template = "default"){
		parent::__construct($view, $page, $template);
	}
		
	private function createContent(){
		
		if ($this->view->noSearch() == true){
			$html = file_get_contents('tpl/'.$this->template.'/regional_search.tpl');
		} else {
			$download = $this->view->getDownloadArguments();
			$args = $this->view->getArguments();
			$data = $this->view->getRegionalView();
			
			$records = count($data);
			$query = $this->view->getSearchQueryRegional(); //is die nog nodig?
			
			if ($this->view->isDownload()){
				self::downloadSearchResult();
			} else if ($records == 0){ //meegeven waarop gezocht werd?
				$html = file_get_contents('tpl/'.$this->template.'/regional_no_search_results.tpl');
				$errormessage = "Your search came back empty.";
				$html = $this->replaceConstant($html, '__INSERT_MESSAGE_HERE__', $errormessage);
				$html = $this->replaceConstant($html, '__PORT__', $args['port']);
				$html = $this->replaceConstant($html, '<option>'.$args['region'].'</option>', '<option selected>'.$args['region'].'</option>');
				$html = $this->replaceConstant($html, '<option>'.$args['country'].'</option>', '<option selected>'.$args['country'].'</option>');
				$html = $this->replaceConstant($html, '__SOUNDCODE__', $args['soundcode']);
				
			} else {	
				$googleMapsTable = $this->view->getParseForMaps();
				
				$html = file_get_contents('tpl/'.$this->template.'/regional_search_results.tpl');
				//create a table (this should be cleaner, get it from a template or smt.)
				$table = "<table class=\"table sortable table-responsive table-condensed\">";
				$table .= "<thead><tr><th>".implode('</th><th>', array_keys(current($data)))."</th></tr></thead>\r\n";
				
				foreach($data as $id => $row){
					//we want our images displayed with fancybox. see above about cleaning this up.
					$row = preg_replace("/(.*\.jpg)/i", "<a class=\"fancybox\" rel=\"group\"  id=\"single2\" href=\"img/scans/$1\" title=\"\">View</a>", $row);
					
					//we want our dates to be sortable :( (and while we're busy let's make them fit the EU date format
					$table .= preg_replace("/<td>(\d\d\d\d)-(\d\d)-(\d\d)/", "<td data-dateformat=\"DD-MM-YYYY\">$3-$2-$1","<tr id=".$id."><td>".implode('</td><td>', array_values($row))."</td></tr>");
					
				}
				
				$html = $this->replaceConstant($html, '__INSERT_TABLE_HERE__', $table);
				$html = $this->replaceConstant($html, '__SEARCH_QUERY__', $query);
				$html = $this->replaceConstant($html, '__DOWNLOAD__', $download);
				$html = $this->replaceConstant($html, '__INSERT_GMAPS_TABLE_HERE__', $googleMapsTable);
			}
		}
		
		return $html;
	}
	
	public function createBody($header = true, $menu = true){
	
		$html = file_get_contents('tpl/'.$this->template.'/body.tpl');
				
		$content = $this->createContent();
		
		$menu = $this->createMenu();
		
		//$html = $this->replaceConstant($html, '__INSERT_HEADER_HERE__', $header);
		$html = $this->replaceConstant($html, '__INSERT_MENU_HERE__', $menu);
		$html = $this->replaceConstant($html, '__INSERT_CONTENT_HERE__', $content);
		
		$this->body = $html;
		return true;
	}
}
?>
