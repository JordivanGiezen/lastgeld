<!DOCTYPE html>
<html lang="nl">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Lastgeld</title>
        <meta name="apple-mobile-web-app-title" content="Lastgeld">
        <link rel="apple-touch-icon" href="img/apple/touch-icon-iphone.png">
        <link rel="apple-touch-icon" sizes="76x76" href="img/apple/touch-icon-ipad.png">
        <link rel="apple-touch-icon" sizes="120x120" href="img/apple/touch-icon-iphone-retina.png">
        <link rel="apple-touch-icon" sizes="152x152" href="img/apple/touch-icon-ipad-retina.png">
        <link rel="shortcut icon" href="img/favicon.ico">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!-- Bootstrap Style -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Table sorting -->
        <link rel="stylesheet" href="css/sortable.css">
        <!-- Custom Style -->
        <link rel="stylesheet" href="css/app.min.css">
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
                
    </head>

    <body>
         <!-- Menu -->
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Navigatie aan/uit</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.html"><img src="img/logo.png" class="img-responsive navbar-brand-image"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                <li class="active"><a href="genealogical.php">Names</a></li>
                <li><a href="regional.php">Places</a></li>
                <li><a href="more.php">More</a></li>
                <li><a href="http://siegfried.webhosting.rug.nl/~shipping/forum/" target="_blank">Forum</a></li>
                <li><a href="info.php">Info</a></li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Lastgeld<b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a target="_blank" href="http://siegfried.webhosting.rug.nl/~shipping/">Database-driven Web Technology</a></li>
                    <li class="divider"></li>
                    <li><a target="_blank" href="/paalgeld_weu">Paalgeld Western Europe</a></li>
                    <li><a target="_blank" href="/paalgeld_win">Paalgeld West Indies</a></li>
                    <li class="divider"></li>
                    <li><a target="_blank" href="http://www.rug.nl/">University of Groningen</a></li>
                  </ul>
                </li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
        
        <!-- Body -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h3>Results for __SEARCH_QUERY__</h3>                    
                        <div class="btn-group btn-group-title">
                            <a href="genealogical.php" class="btn btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
                            <a href="#" class="btn btn-default">Download</a>
                            <a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#103;&#46;&#109;&#46;&#119;&#101;&#108;&#108;&#105;&#110;&#103;&#64;&#114;&#117;&#103;&#46;&#110;&#108;" class="btn btn-default">Report Error</a>
                        </div>
                    </div>               
                </div>            
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <div class="result">
                        <div id="results">
                          <table class="table sortable table-responsive table-condensed">
                            <thead>
                                <tr>
                                    <th>Last name</th>
                                    <th>First name</th>
                                    <th>Region</th>
                                    <th>Country</th>
                                    <th>Begin</th>
                                    <th>End</th>
                                    <th>Entry</th>
                                </tr>
                            </thead>
                              <tbody>
                                <tr>
                                    <td>HAALAMS</td>
                                    <td>FEDDE</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td><a href="record.php">Entry</a></td>
                                </tr>
                                <tr>
                                    <td>MELLES</td>
                                    <td>FEDDE</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td><a href="record.php">Entry</a></td>
                                </tr>
                              </tbody>
                          </table>
                        </div>
                    </div>
            </div>
        </div>
        
        <!-- Bootstrap javascript -->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        <!--Chosen-->
        <script src="js/moment.min.js"></script>
        <script src="js/bootstrap-sortable.js"></script>
        </body>
</html>