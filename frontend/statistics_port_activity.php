<!DOCTYPE html>
<html lang="nl">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Lastgeld</title>
        <meta name="apple-mobile-web-app-title" content="Lastgeld">
        <link rel="apple-touch-icon" href="img/apple/touch-icon-iphone.png">
        <link rel="apple-touch-icon" sizes="76x76" href="img/apple/touch-icon-ipad.png">
        <link rel="apple-touch-icon" sizes="120x120" href="img/apple/touch-icon-iphone-retina.png">
        <link rel="apple-touch-icon" sizes="152x152" href="img/apple/touch-icon-ipad-retina.png">
        <link rel="shortcut icon" href="img/favicon.ico">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!-- Bootstrap Style -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Own Style -->
        <link rel="stylesheet" href="css/app.min.css">
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
                
    </head>

    <body>
         <!-- Menu -->
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Navigatie aan/uit</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.html"><img src="img/logo.png" class="img-responsive navbar-brand-image"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                <li><a href="genealogical.php">Names</a></li>
                <li><a href="regional.php">Places</a></li>
                <li class="active"><a href="more.php">More</a></li>
                <li><a href="http://siegfried.webhosting.rug.nl/~shipping/forum/" target="_blank">Forum</a></li>
                <li><a href="info.php">Info</a></li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Lastgeld<b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a target="_blank" href="http://siegfried.webhosting.rug.nl/~shipping/">Database-driven Web Technology</a></li>
                    <li class="divider"></li>
                    <li><a target="_blank" href="/paalgeld_weu">Paalgeld Western Europe</a></li>
                    <li><a target="_blank" href="/paalgeld_win">Paalgeld West Indies</a></li>
                    <li class="divider"></li>
                    <li><a target="_blank" href="http://www.rug.nl/">University of Groningen</a></li>
                  </ul>
                </li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
        
        <!-- Body -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                      <li><a href="more.php">More</a></li>
                      <li><a href="statistics_port.php">Port Statistics</a></li>
                      <li class="active">Activity</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" id="graph">
                    <div class="well statistics_block">
                        <h2 class="landing_choice">Grafiek</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="well statistics_block">
                        <h2 class="landing_choice">Uitleg</h1>                    
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="well statistics_block">
                        <h2 class="landing_choice">Detailview 1</h1>                    
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="well statistics_block">
                        <h2 class="landing_choice">Detailview 2</h1>                    
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="well statistics_block">
                        <h2 class="landing_choice">Detailview 3</h1>                    
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Bootstrap javascript -->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>