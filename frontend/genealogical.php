<!DOCTYPE html>
<html lang="nl">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Lastgeld</title>
        <meta name="apple-mobile-web-app-title" content="Lastgeld">
        <link rel="apple-touch-icon" href="img/apple/touch-icon-iphone.png">
        <link rel="apple-touch-icon" sizes="76x76" href="img/apple/touch-icon-ipad.png">
        <link rel="apple-touch-icon" sizes="120x120" href="img/apple/touch-icon-iphone-retina.png">
        <link rel="apple-touch-icon" sizes="152x152" href="img/apple/touch-icon-ipad-retina.png">
        <link rel="shortcut icon" href="img/favicon.ico">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!-- Bootstrap Style -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-select.min.css">
        <!-- Own Style -->
        <link rel="stylesheet" href="css/app.min.css">
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
                
    </head>

    <body>
         <!-- Menu -->
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Navigatie aan/uit</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.html"><img src="img/logo.png" class="img-responsive navbar-brand-image"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                <li class="active"><a href="genealogical.php">Names</a></li>
                <li><a href="regional.php">Places</a></li>
                <li><a href="more.php">More</a></li>
                <li><a href="http://siegfried.webhosting.rug.nl/~shipping/forum/" target="_blank">Forum</a></li>
                <li><a href="info.php">Info</a></li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Lastgeld<b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a target="_blank" href="http://siegfried.webhosting.rug.nl/~shipping/">Database-driven Web Technology</a></li>
                    <li class="divider"></li>
                    <li><a target="_blank" href="/paalgeld_weu">Paalgeld Western Europe</a></li>
                    <li><a target="_blank" href="/paalgeld_win">Paalgeld West Indies</a></li>
                    <li class="divider"></li>
                    <li><a target="_blank" href="http://www.rug.nl/">University of Groningen</a></li>
                  </ul>
                </li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
        
        <!-- Body -->
        <!-- Body -->
        <div class="container">
        <div class="row">
    <div class="col-md-12">
        <h2>Genealogical Research</h2>
    </div>
    <div class="col-md-12 clearfix_record"></div>
</div>

<div id="content">
    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
        <li class="active"><a href="#searchform" data-toggle="tab">Search form</a></li>
        <li><a href="#help" data-toggle="tab">Help</a></li>
    </ul>
    <div id="my-tab-content" class="tab-content">
        <div class="tab-pane active" id="searchform">
                    <div class="col-md-12 clearfix_record"></div>
                    <form class="form-horizontal" role="form" action="genealogical_results.php" method="post">
                            <h4>Shipmaster</h4>
                            <div class="form-group">
                                <label for="lastname" class="col-sm-2 control-label">Last name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last name" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="lastname_wc" class="col-sm-2 control-label">Wildcard search</label>
                                <div class="col-sm-10">
                                    <input type="checkbox" id="lastname_wc" name="lastname_wc">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="firstname" class="col-sm-2 control-label">First name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="firstname" name="firstname" placeholder="First name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="firstname_wc" class="col-sm-2 control-label">Wildcard search</label>
                                <div class="col-sm-10">
                                    <input type="checkbox" id="firstname_wc" name="firstname_wc">
                                </div>
                            </div>
                            <hr>
                            <h4>Location</h4>
                            <div class="form-group">
                                <label for="region" class="col-sm-2 control-label">Region</label>
                                <div class="col-sm-10">
                                <select id="region" name="region" class="selectpicker">
                                    <option selected disabled>Region</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                                </div>
                            </div>  
                            <div class="form-group">
                                <label for="country" class="col-sm-2 control-label">Modern Country</label>
                                <div class="col-sm-10">
                                <select id="country" name="country" class="selectpicker">
                                    <option selected disabled>Modern Country</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                                </div>
                            </div>
                            <h4>Timespan</h4>
                            <div class="form-group">
                                <label for="timebegin" class="col-sm-2 control-label">Begin</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="01-04-1744" id="dp1" name="begin">
                                </div>                        
                            </div>
                            <div class="form-group">
                                <label for="timeend" class="col-sm-2 control-label">End</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="31-12-1748" id="dp2" name="end">
                                </div>                        
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10"> 
                                    <button type="submit" name="submit" class="btn btn-default">Submit search</button>
                                </div>
                            </div>      
                        </form>
        </div>
        <div class="tab-pane" id="help">                
                <div class="col-md-12 clearfix_record"></div>
                <div class="col-md-12">
                    <div class="page-header"><h2>Shipmaster</h2></div>
                    <h3>Last name</h3>
                    <p>Enter the last name of a skipper (e.g. Jacob, Johannes). Tick the 'Wildcard search' box to search for near-matches. For example, 'H_nk' finds 'Henk' and 'Hank'.</p>
                    <h3>First name</h3>
                    <p>Enter the first name of a skipper (e.g. Jacob, Johannes). Tick the 'Wildcard search' box to search for near-matches. For example, 'H_nk' finds 'Henk' and 'Hank'.</p>
                    <div class="page-header"><h2>Location</h2></div>
                    <h3>Region</h3>
                    <p>Enter the name of a region or province (e.g. Zeeland, Ventspils)</p>
                    <h3>Modern Country</h3>
                    <p>Enter the name of a modern country (e.g. Latvia, Germany)</p>
                    <div class="page-header"><h2>Timespan</h2></div>
                    <h3>Begin and End</h3>
                    <p>Enter a date between 01-04-1744 and 31-12-1748 (DD-MM-YYYY)</p>
                </div>
        </div>
    </div>
</div>    
        </div>
        
        <!-- Bootstrap javascript -->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        <script src="js/bootstrap-select.min.js"></script>
        <!--Chosen-->
        <script src="js/datepick.js"></script>            
        <script>
		$(function(){
			window.prettyPrint && prettyPrint();
			$('#dp1').datepicker({
				format: 'dd-mm-yyyy',
                todayBtn: 'linked'
                
			});
            
            $('#dp2').datepicker({
				format: 'dd-mm-yyyy',
                todayBtn: 'linked'
			});
          
            $('select').selectpicker();
		});
	   </script>
    </body>
</html>