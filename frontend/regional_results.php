
<!DOCTYPE html>
<html lang="nl">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Regional</title>
        <meta name="apple-mobile-web-app-title" content="Lastgeld">
        <link rel="apple-touch-icon" href="http://lastgeld.jordivangiezen.nl/img/apple/touch-icon-iphone.png">
        <link rel="apple-touch-icon" sizes="76x76" href="http://lastgeld.jordivangiezen.nl/img/apple/touch-icon-ipad.png">
        <link rel="apple-touch-icon" sizes="120x120" href="http://lastgeld.jordivangiezen.nl/img/apple/touch-icon-iphone-retina.png">
        <link rel="apple-touch-icon" sizes="152x152" href="http://lastgeld.jordivangiezen.nl/img/apple/touch-icon-ipad-retina.png">
        <meta name="theme-color" content="#337ab7">
        <link rel="shortcut icon" href="http://lastgeld.jordivangiezen.nl/img/favicon.ico">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!-- Bootstrap Style -->
        <link rel="stylesheet" href="http://lastgeld.jordivangiezen.nl/css/bootstrap.min.css">
        <link rel="stylesheet" href="http://lastgeld.jordivangiezen.nl/css/bootstrap-select.min.css">
        <!-- Table sorting -->
        <link rel="stylesheet" href="http://lastgeld.jordivangiezen.nl/css/sortable.css">
        <!-- Custom Style -->
        <link rel="stylesheet" href="css/app.min.css">
        <link rel="stylesheet" href="http://lastgeld.jordivangiezen.nl/css/jquery.fancybox.css">
        <!-- Google Charts JS -->
        <script type="text/javascript" src="https://www.google.com/jsapi"></script><!-- Die k*tzooi wil niet draaien wanneer de JS aan het eind zit.-->
		  
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
                
    </head>    <body>
		<!-- Menu -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Navigatie aan/uit</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="http://lastgeld.jordivangiezen.nl/Home/"><img src="img/logo.png" class="img-responsive navbar-brand-image"></a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li><a href="http://lastgeld.jordivangiezen.nl/Genealogy/">Names</a></li>
          <li><a href="http://lastgeld.jordivangiezen.nl/Regional/">Places</a></li>
          <li><a href="http://lastgeld.jordivangiezen.nl/More/">More</a></li>
          <li><a href="http://siegfried.webhosting.rug.nl/~shipping/forum/" target="_blank">Forum</a></li>
          <li><a href="http://lastgeld.jordivangiezen.nl/Information/">Info</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Lastgeld<b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a target="_blank" href="http://siegfried.webhosting.rug.nl/~shipping/">Database-driven Web Technology</a></li>
              <li class="divider"></li>
              <li><a target="_blank" href="http://siegfried.webhosting.rug.nl/~shipping/paalgeld_weu">Paalgeld Western Europe</a></li>
              <li><a target="_blank" href="http://siegfried.webhosting.rug.nl/~shipping/paalgeld_win">Paalgeld West Indies</a></li>
              <li class="divider"></li>
              <li><a target="_blank" href="http://www.rug.nl/">University of Groningen</a></li>
            </ul>
          </li>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
        <!-- Wrap all page content here -->
        <!-- Ik heb hier Row weggehaald, omdat we hier en daar wat trucjes met rows moeten toepassen -->
        <div class="container">
            
            <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Report an error</h4>
      </div>
      <form class="form-horizontal" role="form" action="regional_results.php" method="post">
      <div class="modal-body">
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="name" name="name" placeholder="Your name">
            </div>
        </div>
        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="email" name="email" placeholder="Your email">
            </div>
        </div>
        <div class="form-group">
            <label for="problem" class="col-sm-2 control-label">Problem</label>
            <div class="col-sm-10">
                <textarea class="form-control" id="problem" rows="3"></textarea>  
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="submit" name="submit">Send</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>
                    
<div class="row">    
    <div class="col-md-12">                  
            <div class="btn-group btn-group-title">
                <a href="http://lastgeld.jordivangiezen.nl/Regional/" class="btn btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back to search form</a>
                <a href="http://lastgeld.jordivangiezen.nl/Regional/&action=download&port=hamburg" class="btn btn-default">Download</a>
                <a href="#" class="btn btn-default" data-toggle="modal" data-target="#myModal">Report Error</a>
            </div>      
            <div class="maps_hint">
                <p class="mapshint">Click on the <img class="symbol" src="/img/ic_place.png"/></span> in the Google Maps for more information about this port.</p>
            </div>
    </div>           
</div>

<div class="row">
	<div class="col-md-12" style="height: 500px;">
		<div id="map-canvas" style="height: 500px;"></div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
            <div id="results">
                <table class="table sortable table-responsive table-condensed"><thead><tr><th>Captain</th><th>Date</th><th>Cargoes</th><th>Tons</th><th>Tax in Guilders</th><th>Entry</th></tr></thead>
<tr id=11415><td>MARTEN LEVOOGD</td><td data-dateformat="DD-MM-YYYY">10-07-1748</td><td>51</td><td>100.790</td><td>0.3</td><td><a class="fancybox" rel="group"  id="single2" href="img/scans/lg1748-039-040.jpg" title="">View</a></td></tr>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDHmhz1TT9nJ1RH0OlptIxXDiu8THJ7vWI"></script>
    <script>		
		function initialize(table) {
			var infowindow = new google.maps.InfoWindow({
				content: ''
			});
			var bounds = new google.maps.LatLngBounds();
			var start = new google.maps.LatLng(52.373055,4.899722);
			var mapOptions = {
				zoom: 6,
				center: start,
				mapTypeId: google.maps.MapTypeId.TERRAIN
			};
			var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
			var marker = [];
			var contentString = [];
			var position = [];
			var populationOptions = [];
			var cityCircle = [];
			
			for (i in table){
				position[i] = new google.maps.LatLng(table[i].lat, table[i].lng);
		
				contentString[i] = '<div style="min-width: 10em;">'+
					'<b>'+table[i].portName+'</b>'+
					'<p>'+
					'Total departures: ' + table[i].total + '<br />'+
					'Total cargoes: ' + table[i].cargoes + '<br />'+
					'Total tons: ' + table[i].tons + '<br />'+
					'Total tax: ' + table[i].tax + '<br />'+
					'</p>'+
					'</div>';
					
				marker[i] = new google.maps.Marker({
					position: position[i],
					map: map,
					title: table[i].portName,
					html: contentString[i]
				});
				populationOptions[i] = {
					strokeColor: '#FF0000',
					strokeOpacity: 0.8,
					strokeWeight: 2,
					fillColor: '#FF0000',
					fillOpacity: 0.35,
					map: map,
					center: position[i],
					radius: Math.sqrt(table[i].total/100)*20000
				};
				
				cityCircle[i] = new google.maps.Circle(populationOptions[i]);
				
				bounds.extend(marker[i].position);
				
				google.maps.event.addListener(marker[i], 'click', function() {
					infowindow.setContent(this.html);
					infowindow.open(map,this);						
				});
			}
			
			map.fitBounds(bounds);
			var listener = google.maps.event.addListener(map, "idle", function () {
				map.setZoom(6);
				google.maps.event.removeListener(listener);
			});
			
		}
		var table = Array({portName:"Hamburg", lat:"53.55",lng:"10",total:"1",cargoes:"51",tons:"100.790",tax:"0.3"});
		
		google.maps.event.addDomListener(window, 'load', initialize(table));
    </script>

    <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Report an error</h4>
              </div>
              <form class="form-horizontal" role="form" action="regional_results.php" method="post">
              <div class="modal-body">
                
              </div>
              <div class="modal-footer">
                <button class="btn btn-primary" type="submit" name="submit">Send</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
              </form>
            </div>
          </div>
        </div>

        </div>

        <!-- Bootstrap javascript -->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        <script src="js/datepick.min.js"></script>    
        <script src="js/moment.min.js"></script>
        <script src="js/bootstrap-sortable.js"></script>        
        <script src="js/bootstrap-select.min.js"></script> 
        <script src="js/jquery.fancybox.js"></script>
        <script>
        $(document).ready(function() {
            // Set up image viewer
            $(".fancybox").fancybox();
			
              // Set up date pickers
            window.prettyPrint && prettyPrint();
            $('#dp1').datepicker({
                format: 'dd-mm-yyyy',
                todayBtn: 'linked'
                
            });
            
            $('#dp2').datepicker({
                format: 'dd-mm-yyyy',
                todayBtn: 'linked'
            });

            // Set up form dropdown elements
            $('select').selectpicker();
        }); 
        </script>
    </body>
</html>