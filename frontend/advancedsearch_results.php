<!DOCTYPE html>
<html lang="nl">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Lastgeld</title>
        <meta name="apple-mobile-web-app-title" content="Lastgeld">
        <link rel="apple-touch-icon" href="img/apple/touch-icon-iphone.png">
        <link rel="apple-touch-icon" sizes="76x76" href="img/apple/touch-icon-ipad.png">
        <link rel="apple-touch-icon" sizes="120x120" href="img/apple/touch-icon-iphone-retina.png">
        <link rel="apple-touch-icon" sizes="152x152" href="img/apple/touch-icon-ipad-retina.png">
        <link rel="shortcut icon" href="img/favicon.ico">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!-- Bootstrap Style -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Own Style -->
        <link rel="stylesheet" href="css/app.min.css">
        <link rel="stylesheet" href="css/sortable.css">
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
                
    </head>

    <body>
         <!-- Menu -->
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Navigatie aan/uit</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.html"><img src="img/logo.png" class="img-responsive navbar-brand-image"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                <li><a href="genealogical.php">Names</a></li>
                <li><a href="regional.php">Places</a></li>
                <li class="active"><a href="more.php">More</a></li>
                <li><a href="http://siegfried.webhosting.rug.nl/~shipping/forum/" target="_blank">Forum</a></li>
                <li><a href="info.php">Info</a></li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Lastgeld<b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a target="_blank" href="http://siegfried.webhosting.rug.nl/~shipping/">Database-driven Web Technology</a></li>
                    <li class="divider"></li>
                    <li><a target="_blank" href="/paalgeld_weu">Paalgeld Western Europe</a></li>
                    <li><a target="_blank" href="/paalgeld_win">Paalgeld West Indies</a></li>
                    <li class="divider"></li>
                    <li><a target="_blank" href="http://www.rug.nl/">University of Groningen</a></li>
                  </ul>
                </li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
        
        <!-- Body -->
        <div class="container">
 <div class="row">
    <div class="col-md-12">
        <h2>Advanced Search</h2>
        <div class="btn-group btn-group-title">
			<a href="http://lastgeld.jordivangiezen.nl/Advanced/" class="btn btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back to search form</a>
			<a href="http://lastgeld.jordivangiezen.nl/Advanced/&amp;action=download&amp;lastname=jansen&amp;begin=01-04-1744&amp;end=31-12-1748&amp;taxOperator==&amp;tonOperator==&amp;loadsOperator==" class="btn btn-default">Download</a>
			<a href="#" class="btn btn-default" data-toggle="modal" data-target="#myModal">Report Error</a>
		</div>
    </div>
    <div class="col-md-12 clearfix_record"></div>
</div>
            
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Report an error</h4>
  </div>
  <form class="form-horizontal" role="form" action="regional_results.php" method="post">
  <div class="modal-body">
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Name</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="name" name="name" placeholder="Your name">
        </div>
    </div>
    <div class="form-group">
        <label for="email" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="email" name="email" placeholder="Your email">
        </div>
    </div>
    <div class="form-group">
        <label for="problem" class="col-sm-2 control-label">Problem</label>
        <div class="col-sm-10">
            <textarea class="form-control" id="problem" rows="3"></textarea>  
        </div>
    </div>
  </div>
  <div class="modal-footer">
    <button class="btn btn-primary" type="submit" name="submit">Send</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>
  </form>
</div>
</div>
</div>

<div id="content">
    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
        <li class="active"><a href="#searchresults" data-toggle="tab">Search results</a></li>
        <li><a href="#searchform" data-toggle="tab">Search form</a></li>
        <li><a href="#help" data-toggle="tab">Help</a></li>
    </ul>
    <div id="my-tab-content" class="tab-content">
        <div class="tab-pane active" id="searchresults">
            <div class="col-md-12 clearfix"></div>
            <div class="row">
                <div class="col-md-12 mapcol">
                    <div id="map-canvas" class="mapcol"></div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="result">
                        <div id="results">
                            __INSERT_TABLE_HERE__
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="searchform">
                    <div class="col-md-12 clearfix_record"></div>
                    <form class="form-horizontal" role="form" action="" method="post">
                            <div class="col-md-12 clearfix"></div>
                            <h4>Shipmaster</h4>
                            <div class="form-group">
                                <label for="lastname" class="col-sm-2 control-label">Last name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="lastname" placeholder="Last name" name="lastname">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="lastname_wc" class="col-sm-2 control-label">Soundex search</label>
                                <div class="col-sm-10">
                                    <input type="checkbox" id="lastname_wc" name="lastname_wc" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="firstname" class="col-sm-2 control-label">First name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="firstname" name="firstname" placeholder="First name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="firstname_wc" class="col-sm-2 control-label">Soundex search</label>
                                <div class="col-sm-10">
                                    <input type="checkbox" id="firstname_wc" name="firstname_wc" >
                                </div>
                            </div>
                            <hr>
                            <h4>Location</h4>
                            <div class="form-group">
                                <label for="region" class="col-sm-2 control-label">Region</label>
                                <div class="col-sm-10">
                                <select id="region" name="region" class="selectpicker">
                                    <option selected value="">Region</option>
									<option>Hamburg, Bremen, Kleine Oost</option>
									<option>Norway</option>
									<option>South Baltic: East Prussia to Lubeck</option>
									<option>France - Atlantic Coast</option>
									<option>England and Wales</option>
									<option>Russia around St. Petersburg</option>
									<option>Livland</option>
									<option>Sweden and Finland</option>
									<option>Spain</option>
									<option>Portugal</option>
									<option>Russian Arctic ports (Archangel)</option>
									<option>Italy</option>
									<option>Denmark</option>
									<option>Scotland</option>
									<option>Other European and Asian Ports in Medit.</option>
									<option>Africa outside the Mediterranean</option>
									<option>France - Mediterranean Coast</option>
									<option>Kurland</option>
									<option>African Mediterranean Ports</option>
									<option>Greenland and Davis Strait</option>
									<option>Esthonia</option>
									<option>The Austrian Netherlands</option>
									<option>The Netherlands</option>
									<option>Mediterranean - unspecified</option>
									<option>Holstein</option>
									<option>Ireland</option>
									<option>North America</option>
									<option>Unindentifiable Ports</option>
                                </select>
                                </div>
                            </div>  
                            <div class="form-group">
                                <label for="country" class="col-sm-2 control-label">Modern Country</label>
                                <div class="col-sm-10">
                                <select id="country" name="country" class="selectpicker">
                                    <option selected value="">Modern Country</option>
									<option>Germany</option>
									<option>Norway</option>
									<option>France</option>
									<option>United Kingdom</option>
									<option>Russia</option>
									<option>Poland</option>
									<option>Latvia</option>
									<option>Estonia</option>
									<option>Sweden</option>
									<option>Spain</option>
									<option>Portugal</option>
									<option>Italy</option>
									<option>Denmark</option>
									<option>Turkey</option>
									<option>Østersøen</option>
									<option>Greenland</option>
									<option>Lithuania</option>
									<option>Finland</option>
									<option>Belgium</option>
									<option>Greece</option>
									<option>The Netherlands</option>
									<option>Mediterranean</option>
									<option>Ireland</option>
									<option>Egypt</option>
									<option>Tunisia</option>
									<option>UK Crown Colony</option>
									<option>Unknown</option>
                                </select>
                                </div>
                            </div>
                            <hr>
                            <h4>Timespan</h4>
                            <div class="form-group">
                                <label for="timebegin" class="col-sm-2 control-label">Begin</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="01-04-1744" name="begin" id="dp1">
                                </div>                        
                            </div>
                            <div class="form-group">
                                <label for="timeend" class="col-sm-2 control-label">End</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="31-12-1748" name="end" id="dp2">
                                </div>                        
                            </div>
							<hr>
                            <h4>Attributes</h4>
                            <div class="form-group">
                                <label for="tax" class="col-sm-2 control-label">Tax</label>
                                <div class="col-sm-2">
                                        <select id="taxOperator" name="taxOperator" class="selectpicker">
                                            <option value="="selected>Exact</option>
                                            <option value=">">More than</option>
                                            <option value="<">Less than</option>
                                            <option value="Between">Between</option>
                                        </select>
                                </div>
                                <div class="col-sm-4">  
                                        <input type="text" class="form-control" id="firstfield" placeholder="" name="tax">
                                </div>
                                <div class="col-sm-4" id="taxSecond">
                                        <input type="text" class="form-control" id="taxMax" placeholder="" name="taxMax">
                                </div> 								
                            </div>
                            <div class="form-group">
                                <label for="tonnage" class="col-sm-2 control-label">Tonnage</label>
                                <div class="col-sm-2">
                                        <select id="tonOperator" name="tonOperator" class="selectpicker">
                                            <option value="="selected>Exact</option>
                                            <option value=">">More than</option>
                                            <option value="<">Less than</option>
                                            <option value="Between">Between</option>
                                        </select>
                                </div>
                                <div class="col-sm-4">  
                                        <input type="text" class="form-control" id="firstfield" placeholder="" name="ton">
                                </div>
                                <div class="col-sm-4" id="tonSecond">
                                        <input type="text" class="form-control" id="tonMax" placeholder="" name="tonMax">
                                </div> 								
                            </div> 
                            <div class="form-group">
                                <label for="loads" class="col-sm-2 control-label">Loads</label>
                                <div class="col-sm-2">
                                        <select id="loadsOperator" name="loadsOperator" class="selectpicker">
                                            <option value="="selected>Exact</option>
                                            <option value=">">More than</option>
                                            <option value="<">Less than</option>
                                            <option value="Between">Between</option>
                                        </select>
                                </div>
                                <div class="col-sm-4">  
                                        <input type="text" class="form-control" id="firstfield" placeholder="" name="loads">
                                </div>
                                <div class="col-sm-4" id="loadsSecond">
                                        <input type="text" class="form-control" id="loadsMax" placeholder="" name="loadsMax">
                                </div> 								
                            </div> 
							<hr>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10"> 
                                    <button type="submit" name="submit" class="btn btn-default">Submit search</button>
                                </div>
                            </div> 							
                        </form>
        </div>
        <div class="tab-pane" id="help">                
                <div class="col-md-12 clearfix_record"></div>
                <div class="col-md-12">
                    <div class="page-header"><h2>Shipmaster</h2></div>
                    <h3>Last name</h3>
                    <p>Enter the last name of a skipper (e.g. De Jong, Molman). Tick the 'Wildcard search' box to search for near-matches. For example, 'Jan_en' finds 'Jansen' and 'Janzen'.</p>
                    <h3>First name</h3>
                    <p>Enter the first name of a skipper (e.g. Jacob, Johannes). Tick the 'Wildcard search' box to search for near-matches. For example, 'H_nk' finds 'Henk' and 'Hank'.</p>
                    <div class="page-header"><h2>Location</h2></div>
                    <h3>Port</h3>
                    <p>Enter the name of a city (e.g. Hamburg, Stockholm).</p>
                    <h3>Region</h3>
                    <p>Pick a region or province (e.g. Zeeland, Ventspils).</p>
                    <h3>Modern Country</h3>
                    <p>Pick a modern country (e.g. Latvia, Germany).</p>
                    <h3>Sound Code</h3>
                    <p>Enter the code of a port, as used by the Sound Toll project (e.g. 741KLO, 421DAZ).</p>
                    <div class="page-header"><h2>Timespan</h2></div>
                    <h3>Begin and End</h3>
                    <p>Enter a date between 01-04-1744 and 31-12-1748 (DD-MM-YYYY)</p>
                    <div class="page-header"><h2>Load</h2></div>
                    <h3>Tonnage</h3>
                    <p>Enter the load weight in metric tonnes.</p>
                    <h3>Loads</h3>
                    <p>Enter the load in loads.</p>
                    <div class="page-header"><h2>Tax</h2></div>
                    <h3>Amount</h3>
                    <p>Enter the amount of tax paid, with at most two decimals.</p>
                </div>
        </div>
    </div>
</div>

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDHmhz1TT9nJ1RH0OlptIxXDiu8THJ7vWI"></script>
    <script>        
        function initialize(table) {
            var infowindow = new google.maps.InfoWindow({
                content: ''
            });
            var bounds = new google.maps.LatLngBounds();
            var start = new google.maps.LatLng(52.373055,4.899722);
            var mapOptions = {
                zoom: 6,
                center: start,
                mapTypeId: google.maps.MapTypeId.TERRAIN
            };
            var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
            var marker = [];
            var contentString = [];
            var position = [];
            
            for (i in table){
                position[i] = new google.maps.LatLng(table[i].lat, table[i].lng);
        
                contentString[i] = '<div style="min-width: 10em;">'+
                    '<b>'+table[i].portName+'</b>'+
                    '<p>'+
                    'Total departures: ' + table[i].total + '<br />'+
                    'Total cargoes: ' + table[i].cargoes + '<br />'+
                    'Total tons: ' + table[i].tons + '<br />'+
                    'Total tax: ' + table[i].tax + '<br />'+
                    '</p>'+
                    '</div>';
                    
                marker[i] = new google.maps.Marker({
                    position: position[i],
                    map: map,
                    title: table[i].portName,
                    html: contentString[i]
                });
                
                bounds.extend(marker[i].position);
                
                google.maps.event.addListener(marker[i], 'click', function() {
                    infowindow.setContent(this.html);
                    infowindow.open(map,this);                      
                });
            }
            
            map.fitBounds(bounds);
            var listener = google.maps.event.addListener(map, "idle", function () {
                if (map.getZoom() > 6){
                    map.setZoom(6);
                }
                google.maps.event.removeListener(listener);
            });
            
        }
        var table = Array(__INSERT_GMAPS_TABLE_HERE__);
        
        google.maps.event.addDomListener(window, 'load', initialize(table));
    </script>
        </div>
        
        <!-- Bootstrap javascript -->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        <script src="js/moment.min.js"></script>
        <script src="js/bootstrap-sortable.js"></script>
        </body>
</html>