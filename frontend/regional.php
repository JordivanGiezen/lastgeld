<!DOCTYPE html>
<html lang="nl">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Lastgeld</title>
        <meta name="apple-mobile-web-app-title" content="Lastgeld">
        <link rel="apple-touch-icon" href="img/apple/touch-icon-iphone.png">
        <link rel="apple-touch-icon" sizes="76x76" href="img/apple/touch-icon-ipad.png">
        <link rel="apple-touch-icon" sizes="120x120" href="img/apple/touch-icon-iphone-retina.png">
        <link rel="apple-touch-icon" sizes="152x152" href="img/apple/touch-icon-ipad-retina.png">
        <link rel="shortcut icon" href="img/favicon.ico">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!-- Bootstrap Style -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-select.min.css">
        <!-- Own Style -->
        <link rel="stylesheet" href="css/app.min.css">
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
                
    </head>

    <body>
         <!-- Menu -->
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Navigatie aan/uit</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.html"><img src="img/logo.png" class="img-responsive navbar-brand-image"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                <li><a href="genealogical.php">Names</a></li>
                <li class="active"><a href="regional.php">Places</a></li>
                <li><a href="more.php">More</a></li>
                <li><a href="http://siegfried.webhosting.rug.nl/~shipping/forum/" target="_blank">Forum</a></li>
                <li><a href="info.php">Info</a></li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Lastgeld<b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a target="_blank" href="http://siegfried.webhosting.rug.nl/~shipping/">Database-driven Web Technology</a></li>
                    <li class="divider"></li>
                    <li><a target="_blank" href="/paalgeld_weu">Paalgeld Western Europe</a></li>
                    <li><a target="_blank" href="/paalgeld_win">Paalgeld West Indies</a></li>
                    <li class="divider"></li>
                    <li><a target="_blank" href="http://www.rug.nl/">University of Groningen</a></li>
                  </ul>
                </li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
        
        <!-- Body -->
        <!-- Body -->
        <div class="container">
       <div class="row">
    <div class="col-md-12">
        <h2>Regional Research</h2>
    </div>
    <div class="col-md-12 clearfix_record"></div>
</div>
            
<div id="content">
    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
        <li class="active"><a href="#searchform" data-toggle="tab">Search form</a></li>
        <li><a href="#help" data-toggle="tab">Help</a></li>
    </ul>
    <div id="my-tab-content" class="tab-content">
        <div class="tab-pane active" id="searchform">
                    <div class="col-md-12 clearfix_record"></div>
                    <form class="form-horizontal" role="form" action="regional_results.php" method="post">
                <div class="form-group">
                    <label for="port" class="col-sm-2 control-label">Port</label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" id="port" name="port" placeholder="Port">
                    </div>
                </div>
                <div class="form-group">
                    <label for="region" class="col-sm-2 control-label">Region</label>
                    <div class="col-sm-10">
                    <select id="region" name="region" class="selectpicker" selected="Region">
                        <option selected disabled>Region</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                    </select>
                    </div>
                </div>  
                <div class="form-group">
                    <label for="country" class="col-sm-2 control-label">Modern Country</label>
                    <div class="col-sm-10">
                    <select id="country" name="country" class="selectpicker">
                        <option selected disabled>Modern Country</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="soundcode" class="col-sm-2 control-label">Sound Code</label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" id="soundcode" name="soundcode" placeholder="Sound code">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10"> 
                        <button type="submit" name="submit" class="btn btn-default">Submit search</button>
                    </div>
                </div>      
            </form>
        </div>
        <div class="tab-pane" id="help">                
                <div class="col-md-12 clearfix_record"></div>
                <div class="col-md-12">
                    <h3>Port</h3>
                    <p>Enter the name of a city (e.g. Hamburg, Stockholm).</p>
                    <h3>Region</h3>
                    <p>Enter the name of a region or province (e.g. Zeeland, Ventspils).</p>
                    <h3>Modern Country</h3>
                    <p>Enter the name of a modern country (e.g. Latvia, Germany).</p>
                    <h3>Sound Code</h3>
                    <p>Enter the code of a port, as used by the Sound Toll project (e.g. 741KLO, 421DAZ).</p>
                </div>
        </div>
    </div>
</div>
        </div>
        
        <!-- Bootstrap javascript -->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        <script src="js/bootstrap-select.min.js"></script>
        <!--Chosen-->
        <script src="js/datepick.min.js"></script>            
        <script>
		$(function(){
			window.prettyPrint && prettyPrint();
			$('#dp1').datepicker({
				format: 'dd-mm-yyyy',
                todayBtn: 'linked'
                
			});
            
            $('#dp2').datepicker({
				format: 'dd-mm-yyyy',
                todayBtn: 'linked'
			});
            
            $('select').selectpicker();
		});
	   </script>
    </body>
</html>