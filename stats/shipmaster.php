<?php
/**
 * A php script that generates JSON strings to be used with the Google Charts API service. All the data they give back
 * are about tons.
 */
	header('Content-Type: text/html; charset=utf-8');
	///////////////////
	// Initialisatie //
	///////////////////

	// Load configfile
	require_once('../config/config.php'); 
	
	// Load MySQL class
	require_once('../classes/mysql.class.php');

	// Load export functions
	require_once('functions.php');

	// Connect to the database and kill when it errors 
	$db = new MySQL(true, $mysql['database'], $mysql['server'], $mysql['user'], $mysql['password']);
	if ($db->Error()) $db->Kill();

	/**
	* WARNING! UNTESTED CODE AHEAD! 
	*/

	/**
	*	SHIPMASTER: ACTIVITY
	* Query to retrieve the most active shipmasters
	*/
	if($_GET['action'] == "mostactiveshipmaster"){
		$toQuery = "SELECT CONCAT_WS(' ',firstNameStandard,lastNameStandard) as shipmaster, COUNT(*) as trips 
					FROM lastgeld 
					GROUP BY shipmaster 
					ORDER BY trips desc";
		$result = $db->Query($toQuery);
		$queryres = array_slice($db->getArray(), 0, 20);
			
		$colnames = array("shipmaster", "trips");
		$types = array(
			"shipmaster" => "string",
			"trips" => "number"
			);

		echo getGoogleChartJSON($colnames, $types, $queryres);
		return;
	}
	

	if($_GET['action'] == "mostactiveshipmaster" && isset($_GET['year'])){
		if(isValidYear()){
			$toQuery = "SELECT CONCAT_WS(' ',firstNameStandard,lastNameStandard) as shipmaster, COUNT(*) as trips 
						FROM lastgeld 
						WHERE EXTRACT(YEAR FROM date) = " . $_GET['year'] . "
						GROUP BY shipmaster 
						ORDER BY trips desc";
			$result = $db->Query($toQuery);
			$queryres = array_slice($db->getArray(), 0, 20);
			
			$colnames = array("shipmaster", "trips");
			$types = array(
				"shipmaster" => "string",
				"trips" => "number"
			);


			echo getGoogleChartJSON($colnames, $types, $queryres);
		} else {
			echo "Request did not meet the requirements";
		}
		return;
	}

	/**
	*	SHIPMASTER: VALUE
	* Query to retrieve the shipmasters carrying the most valuables
	*/
	if($_GET['action'] == "valuepershipmaster" && isset($_GET['year'])){
		if(isValidYear()){

			$toQuery = 'SELECT CONCAT_WS(" ",firstNameStandard,lastNameStandard) as shipmaster, ROUND(SUM(tons),2) as Value
						FROM lastgeld
						WHERE EXTRACT(YEAR FROM date) = ' . $_GET['year'] . ' 
						GROUP BY shipmaster
						ORDER BY Value DESC';
			$result = $db->Query($toQuery);
			$queryres = array_slice($db->getArray(), 0, 20);
			$colnames = array("shipmaster", "Value");
			$types = array(
				"shipmaster" => "string",
				"Value" => "number"
				);

			echo getGoogleChartJSON($colnames, $types, $queryres);
		} else {
			echo "Request did not meet the requirements";
		}
		return;
	}

	/**
	 * Generates a chart to represent which ports had the most transactions in the lastgeld register for the given year
	 */
	if($_GET['action'] == "valuepershipmaster"){
		$toQuery = 'SELECT CONCAT_WS(" ",firstNameStandard,lastNameStandard) as shipmaster, ROUND(SUM(tons),2) as Value
						FROM lastgeld
						GROUP BY shipmaster
						ORDER BY Value DESC';

		$result = $db->Query($toQuery);
		$queryres = array_slice($db->getArray(), 0, 20);
		
		$colnames = array("shipmaster", "Value");
		$types = array(
			"shipmaster" => "string",
			"Value" => "number"
			);

		echo getGoogleChartJSON($colnames, $types, $queryres);

		return;
	}

	/**
	*	SHIPMASTER: NUMBER OF SHIPMASTERS
	* Query to retrieve the number of shipmasters per country
	*/
	if($_GET['action'] == "shipmasterspercountry" && isset($_GET['year'])){
		if(isValidYear()){
			// #HeleWalgelijkeQuery
			$toQuery = 'SELECT countryNow, COUNT(shipmaster) as totalShipmasters 
						FROM (SELECT DISTINCT(CONCAT_WS(" ",firstNameStandard,lastNameStandard)) as shipmaster, date, portCode FROM lastgeld) as distinctShipmasters 
						NATURAL JOIN ports 
						WHERE EXTRACT(YEAR FROM date) = ' . $_GET['year'] . ' 
						GROUP BY countryNow ORDER BY totalShipmasters DESC';
			$result = $db->Query($toQuery);
			$queryres = array_slice($db->getArray(), 0, 20);
			$colnames = array("shipmaster", "Number of Shipmasters");
			$types = array(
				"countryNow" => "string",
				"totalShipmasters" => "number"
				);

			echo getGoogleChartJSON($colnames, $types, $queryres);
		} else {
			echo "Request did not meet the requirements";
		}
		return;
	}

	if($_GET['action'] == "shipmasterspercountry"){
		// #HeleWalgelijkeQueryAGAIN
		$toQuery = 'SELECT countryNow, COUNT(shipmaster) as totalShipmasters 
					FROM (SELECT DISTINCT(CONCAT_WS(" ",firstNameStandard,lastNameStandard)) as shipmaster, portCode FROM lastgeld) as distinctShipmasters 
					NATURAL JOIN ports 
					GROUP BY countryNow 
					ORDER BY totalShipmasters DESC';
		$result = $db->Query($toQuery);
		$queryres = array_slice($db->getArray(), 0, 20);
		
		$colnames = array("Modern country", "Number of Shipmasters");
		$types = array(
			"countryNow" => "string",
			"totalShipmasters" => "number"
			);

		echo getGoogleChartJSON($colnames, $types, $queryres);

		return;
	}