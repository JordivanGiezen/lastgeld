<?php

/**
 * A PHP script that contains various functions that are used by the PHP scripts that generate statisics.
 */


/**
 * Creates a JSON that can be used for google charts. It is assumed that the cols, type and data order are equal
 *
 * @param $cols An array that contains the column names.
 * @param $type An array that contains the types of the values
 * @param $data An array of arrays containg the data that needs to be entered into the JSON
 * @return string A JSON string that is interpertable for the Google Charts API
 */
function getGoogleChartJSON($cols, $type, $data){
		$json;

		if(is_null($cols) || is_null($data)){
			return $json;
		}

		$json = "{";

		// Start creating the columns in the JSON.
		$json .= '"cols": [';
		foreach ($cols as $key => $value) {
			$json .= '{"id":"","label":"' . $value . '","pattern":"","type":"' . $type[$value] . '"},';
		}
		$json .= '], "rows": [';
		foreach ($data as $pos => $subdata) {
			$json .= '{"c":[';
			foreach ($subdata as $key => $value) {
				if ($type[$key] == "number" ) {
					$json .= '{"v": ' . $value . ' ,"f":null},';
				} else {
					$json .= '{"v": "' . $value . '" ,"f":null},';
				}
				
			}
			$json .= ']},';
		}
		$json .= ']}';

		$json = str_replace("},]", "}]", $json);
		return $json;
	}

/**
 * Checks if the variable year is numeric and if it lies between 1744 and 1748 (the years in which the lastgeld register
 * was maintained.
 *
 * @return bool if the variable year is numeric and if it lies between 1744 and 1748
 */
function isValidYear(){
		return is_numeric($_GET['year']) && $_GET['year'] >= 1744 && $_GET['year'] <= 1748;
	}

?>