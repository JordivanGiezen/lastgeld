<?php

/**
 * A php script that generates JSON strings to be used with the Google Charts API service. All the data they give back
 * are about ports, their appearences in the lastgeld register and their wealth/taxes
 */
	header('Content-Type: text/html; charset=utf-8');
	///////////////////
	// Initialisatie //
	///////////////////

	// Load configfile
	require_once('../config/config.php'); 
	
	// Load MySQL class
	require_once('../classes/mysql.class.php');

	// Load export functions
	require_once('functions.php');

	// Connect to the database and kill when it errors 
	$db = new MySQL(true, $mysql['database'], $mysql['server'], $mysql['user'], $mysql['password']);
	if ($db->Error()) $db->Kill();
	
	/**
	 * Query to get information for the amount of ports per country.
	 */
	if($_GET['action'] == "mostportspercountry"){
		$toQuery = 'SELECT countryNow as Country, COUNT(DISTINCT portCode) as Amount
					FROM ports
					WHERE countryNow != ""
					GROUP BY countryNow
					ORDER BY Amount DESC';

		$result = $db->Query($toQuery);
		$queryres = array_slice($db->getArray(), 0, 20);
		
		$colnames = array("Country", "Amount");
		$types = array(
			"Country" => "string",
			"Amount" => "number"
			);

		echo getGoogleChartJSON($colnames, $types, $queryres);

		return;
	}

	/**
	*	Query to give information which port had the most transactions in the lastgeld register for the given year
	*/
	if($_GET['action'] == "amountperport" && isset($_GET['year'])){
		if(isValidYear()){

			$toQuery = 'SELECT departurePort AS Port, COUNT(departurePort) as Amount
						FROM `lastgeld` 
						WHERE departurePort != "" AND EXTRACT(YEAR FROM date) = ' . $_GET['year'] . '
						GROUP BY departurePort
						ORDER BY Amount DESC';
			$result = $db->Query($toQuery);
			$queryres = array_slice($db->getArray(), 0, 20);
			$colnames = array("Port", "Amount");
			$types = array(
				"Port" => "string",
				"Amount" => "number"
				);

			echo getGoogleChartJSON($colnames, $types, $queryres);
		} else {
			echo "Request did not meet the requirements";
		}
		return;
	}

	/**
	*	Chart to give information which country had the most transactions in the lastgeld register for the given year
	*/
	if($_GET['action'] == "amountpercountry" && isset($_GET['year'])){
		if(isValidYear()){

		$toQuery = 'SELECT countryNow as Country, COUNT(lastgeld.portCode) AS Amount
					FROM lastgeld INNER JOIN ports ON lastgeld.portCode = ports.portCode
					WHERE EXTRACT(YEAR FROM date) = ' . $_GET['year'] . ' 
					GROUP BY Country
					ORDER BY Amount DESC';
			$result = $db->Query($toQuery);
			$queryres = array_slice($db->getArray(), 0, 20);
			$colnames = array("Country", "Amount");
			$types = array(
				"Country" => "string",
				"Amount" => "number"
				);

			echo getGoogleChartJSON($colnames, $types, $queryres);
		} else {
			echo "Request did not meet the requirements";
		}
		return;
	}


	/**
	 * Query to find which country had the most transactions in the lastgeld register
	 */
	if($_GET['action'] == "amountpercountry"){
		$toQuery = 'SELECT countryNow as Country, COUNT(lastgeld.portCode) AS Amount
					FROM lastgeld INNER JOIN ports ON lastgeld.portCode = ports.portCode
					GROUP BY Country
					ORDER BY Amount DESC';

		$result = $db->Query($toQuery);
		$queryres = array_slice($db->getArray(), 0, 20);
		
		$colnames = array("Country", "Amount");
		$types = array(
			"Country" => "string",
			"Amount" => "number"
			);

		echo getGoogleChartJSON($colnames, $types, $queryres);

		return;
	}

	/**
	 * Query to find which ports had the most transactions in the lastgeld register
	 */
	if($_GET['action'] == "amountperport"){
		$toQuery = 'SELECT departurePort AS Port, COUNT(departurePort) as Amount
					FROM `lastgeld` 
					WHERE departurePort != ""
					GROUP BY departurePort
					ORDER BY Amount DESC';

		$result = $db->Query($toQuery);
		$queryres = array_slice($db->getArray(), 0, 20);
		
		$colnames = array("Port", "Amount");
		$types = array(
			"Port" => "string",
			"Amount" => "number"
			);

		echo getGoogleChartJSON($colnames, $types, $queryres);

		return;
	}

	/**
	*	Query to give information which port had the most valueables on board according to the lastgeld register
	*/
	if($_GET['action'] == "valueperport" && isset($_GET['year'])){
		if(isValidYear()){

			$toQuery = 'SELECT portCode, ROUND(SUM(tons),2) as Value
						FROM lastgeld
						WHERE EXTRACT(YEAR FROM date) = ' . $_GET['year'] . ' 
						GROUP BY portCode
						ORDER BY Value DESC';
			$result = $db->Query($toQuery);
			$queryres = array_slice($db->getArray(), 0, 20);
			$colnames = array("portCode", "Value");
			$types = array(
				"portCode" => "string",
				"Value" => "number"
				);

			echo getGoogleChartJSON($colnames, $types, $queryres);
		} else {
			echo "Request did not meet the requirements";
		}
		return;
	}

	/**
	 * Generates a chart to represent which ports had the most transactions in the lastgeld register for the given year
	 */
	if($_GET['action'] == "valueperport"){
		$toQuery = 'SELECT portCode, ROUND(SUM(tons),2) as Value
						FROM lastgeld
						GROUP BY portCode
						ORDER BY Value DESC';

		$result = $db->Query($toQuery);
		$queryres = array_slice($db->getArray(), 0, 20);
		
		$colnames = array("portCode", "Value");
		$types = array(
			"portCode" => "string",
			"Value" => "number"
			);

		echo getGoogleChartJSON($colnames, $types, $queryres);

		return;
	}
?>