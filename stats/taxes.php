<?php

/**
 * A php script that generates JSON strings to be used with the Google Charts API service. All the data given back
 * are about taxes.
 */
	header('Content-Type: text/html; charset=utf-8');
	///////////////////
	// Initialisatie //
	///////////////////

	// Load configfile
	require_once('../config/config.php'); 
	
	// Load MySQL class
	require_once('../classes/mysql.class.php');

	// Load export functions
	require_once('functions.php');

	// Connect to the database and kill when it errors 
	$db = new MySQL(true, $mysql['database'], $mysql['server'], $mysql['user'], $mysql['password']);
	if ($db->Error()) $db->Kill();

	/**
	*	Query to give information which shipmaster paid the most lastgeld for the given year
	*/
	if($_GET['action'] == "mosttaxespayedshipmaster" && isset($_GET['year'])){
		if(isValidYear()){
			$toQuery = "SELECT CONCAT(firstNameStandard, ' ', lastNameStandard) AS fullNameStandard, ROUND(SUM(taxGuilders), 1) AS Tax
						FROM lastgeld
						WHERE EXTRACT(YEAR FROM date) = " . $_GET['year'] . "
						GROUP BY fullNameStandard
						ORDER BY Tax DESC";
			$result = $db->Query($toQuery);
			$queryres = array_slice($db->getArray(), 0, 20);
			
			$colnames = array("fullNameStandard", "Tax");
			$types = array(
				"fullNameStandard" => "string",
				"Tax" => "number"
				);

			echo getGoogleChartJSON($colnames, $types, $queryres);
		} else {
			echo "Request did not meet the requirements";
		}
		return;
	}

	/**
	*	Query to give information which shipmaster paid the most lastgeld
	*/
	if($_GET['action'] == "mosttaxespayedshipmaster"){
		// Query to calculate the sum of taxes paid per standardized name

		$toQuery = "SELECT CONCAT(firstNameStandard, ' ', lastNameStandard) AS fullNameStandard, ROUND(SUM(taxGuilders), 1) AS Tax
								FROM lastgeld
								GROUP BY fullNameStandard
								ORDER BY Tax DESC";
		$result = $db->Query($toQuery);
		$queryres = array_slice($db->getArray(), 0, 20);
		
		$colnames = array("fullNameStandard", "Tax");
		$types = array(
			"fullNameStandard" => "string",
			"Tax" => "number"
			);

		echo getGoogleChartJSON($colnames, $types, $queryres);
		return;
	}

	/**
	*	Query to give information which country paid the most lastgeld for the given year
	*/
	if($_GET['action'] == "mosttaxespayedcountry" && isset($_GET['year'])){
		if(isValidYear()){
			$toQuery = "SELECT countryNow as Country, ROUND(SUM(lastgeld.taxGuilders), 1) as Tax
						FROM lastgeld INNER JOIN ports ON lastgeld.portCode = ports.portCode
						WHERE EXTRACT(YEAR FROM date) = " . $_GET['year'] . "
						GROUP BY Country
						ORDER BY Tax DESC";

			$result = $db->Query($toQuery);
			$queryres = array_slice($db->getArray(), 0, 20);
			$colnames = array("Country", "Tax");
			$types = array(
				"Country" => "string",
				"Tax" => "number"
				);

			echo getGoogleChartJSON($colnames, $types, $queryres);
		} else {
			echo "Request did not meet the requirements";
		}
		return;
	}

	/**
	*	Query to give information which country paid the most lastgeld
	*/
	if($_GET['action'] == "mosttaxespayedcountry"){
		$toQuery = "SELECT countryNow as Country, ROUND(SUM(lastgeld.taxGuilders), 1) as Tax
					FROM lastgeld INNER JOIN ports ON lastgeld.portCode = ports.portCode
					GROUP BY Country
					ORDER BY Tax DESC";

		$result = $db->Query($toQuery);
		$queryres = array_slice($db->getArray(), 0, 20);
		
		$colnames = array("Country", "Tax");
		$types = array(
			"Country" => "string",
			"Tax" => "number"
			);

		echo getGoogleChartJSON($colnames, $types, $queryres);

		return;
	}

	/**
	*	Query to get which country has the most ports.
	*/
	if($_GET['action'] == "mosttaxespayedcountry"){
		$toQuery = "SELECT countryNow as Country, ROUND(SUM(lastgeld.taxGuilders), 1) as Tax
					FROM lastgeld INNER JOIN ports ON lastgeld.portCode = ports.portCode
					GROUP BY Country
					ORDER BY Tax DESC";

		$result = $db->Query($toQuery);
		$queryres = array_slice($db->getArray(), 0, 20);
		
		$colnames = array("Country", "Tax");
		$types = array(
			"Country" => "string",
			"Tax" => "number"
			);

		echo getGoogleChartJSON($colnames, $types, $queryres);

		return;
	}

	/**
	*	Chart info to give information which port paid the most taxes in the lastgeld register for the given year
	*/
	if($_GET['action'] == "taxperport" && isset($_GET['year'])){
		if(isValidYear()){
		// Query to determine the most taxes payed per port
			$toQuery = 'SELECT portCode, ROUND(SUM(taxGuilders),1) as Tax
						FROM lastgeld
						WHERE EXTRACT(YEAR FROM date) = ' . $_GET['year'] . ' 
						GROUP BY portCode
						ORDER BY Tax DESC';
						// Same query, but then with their port cities, which is not unique, because one city may have multiple ports
						// SELECT portName, ROUND(SUM(taxGuilders),1) as Tax
						// FROM lastgeld INNER JOIN ports ON lastgeld.portCode = ports.portCode
						// GROUP BY lastgeld.portCode
						// ORDER BY Tax desc

			$result = $db->Query($toQuery);
			$queryres = array_slice($db->getArray(), 0, 20);
			$colnames = array("portCode", "Tax");
			$types = array(
				"portCode" => "string",
				"Tax" => "number"
				);

			echo getGoogleChartJSON($colnames, $types, $queryres);
		} else {
			echo "Request did not meet the requirements";
		}
		return;
	}

	/**
	*	Chart info to give information which port paid the most taxes in the lastgeld register
	*/
	if($_GET['action'] == "taxperport"){
		// Query to determine the most taxes payed per port
		$toQuery = 'SELECT portCode, ROUND(SUM(taxGuilders),1) as Tax
					FROM lastgeld
					GROUP BY portCode
					ORDER BY Tax DESC';
					// Same query, but then with their port cities, which is not unique, because one city may have multiple ports
					// SELECT portName, ROUND(SUM(taxGuilders),1) as Tax
					// FROM lastgeld INNER JOIN ports ON lastgeld.portCode = ports.portCode
					// GROUP BY lastgeld.portCode
					// ORDER BY Tax desc				

		$result = $db->Query($toQuery);
		$queryres = array_slice($db->getArray(), 0, 20);
		$colnames = array("portCode", "Tax");
		$types = array(
			"portCode" => "string",
			"Tax" => "number"
			);

		echo getGoogleChartJSON($colnames, $types, $queryres);
		return;
	} 
?>