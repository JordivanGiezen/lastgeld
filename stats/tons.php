<?php

/**
 * A php script that generates JSON strings to be used with the Google Charts API service. All the data they give back
 * are about tons.
 */
	header('Content-Type: text/html; charset=utf-8');
	///////////////////
	// Initialisatie //
	///////////////////

	// Load configfile
	require_once('../config/config.php'); 
	
	// Load MySQL class
	require_once('../classes/mysql.class.php');

	// Load export functions
	require_once('functions.php');

	// Connect to the database and kill when it errors 
	$db = new MySQL(true, $mysql['database'], $mysql['server'], $mysql['user'], $mysql['password']);
	if ($db->Error()) $db->Kill();

	/**
	 * Query to give information about which shipmaster moved the most tons for the given year
	 */
	if($_GET['action'] == "tonspershipmaster" && isset($_GET['year'])){
		if(isValidYear()){
			$toQuery = "SELECT fullNameCaptain, SUM(tons) AS totalTons
						FROM lastgeld
						WHERE EXTRACT(YEAR FROM date) = " . $_GET['year'] . "
						GROUP BY fullNameCaptain
						ORDER BY totalTons DESC";
			$result = $db->Query($toQuery);
			$queryres = array_slice($db->getArray(), 0, 20);
			
			$colnames = array("fullNameCaptain", "totalTons");
			$types = array(
				"fullNameCaptain" => "string",
				"totalTons" => "number"
				);

			echo getGoogleChartJSON($colnames, $types, $queryres);
		} else {
			echo "Request did not meet the requirements";
		}
		return;
	}

	/**
	 * Query to give information about which shipmaster moved the most tons
	 */
	if($_GET['action'] == "pershipmaster"){
		$toQuery = "SELECT fullNameCaptain, SUM(tons) AS totalTons
					FROM lastgeld
					GROUP BY fullNameCaptain
					ORDER BY totalTons DESC";
		$result = $db->Query($toQuery);
		$queryres = array_slice($db->getArray(), 0, 20);
			
		$colnames = array("fullNameCaptain", "totalTons");
		$types = array(
			"fullNameCaptain" => "string",
			"totalTons" => "number"
			);

		echo getGoogleChartJSON($colnames, $types, $queryres);
		return;
	}


?>