<?php

	//CONFIG FILE

	//CONFIGS
	$mysql = array(
				'user' => 'root', 
				'password' => 'PASSWORD', 
				'server' => 'localhost', 
				'database' => 'DATABASE'
				);
	
	//REWRITE? 
	define('URL_REWRITE', false); //Looks fancy when it works

	// Force HTTPS?
	define('FORCE_HTTPS', false); //Should be true. 

	
	//SETTINGS
	
	date_default_timezone_set('Europe/Amsterdam'); //Dutch website
	ini_set('memory_limit', 134217728); //Just to be sure
	ini_set('default_charset', 'UTF-8'); //standard in PHP > 5.6.0
	ini_set('display_errors', 'E_ALL & ~E_NOTICE'); //disable errors in live versions!!
	
?>