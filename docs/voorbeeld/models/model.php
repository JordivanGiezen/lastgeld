<?php

class Model {
	
	private $db = "";
	private $page = "";
	private $header = "";
	private $menu = "";
	private $agenda = "";
	
	public function __construct($db){
		$this->db = $db;	//No setter since models shouldn't be used on more than one database and this one should also not be used without a db.	
	}

	public function retrievePage($page){
		$this->db->selectRows("pages", array('pageName' => '\''.$page.'\''));
		if ($this->db->Error()) $this->db->Kill();
		$this->setPage($this->db->RecordsArray());
		return true;
	}
	
	public function getPage(){
		return $this->page;
	}
	
	private function setPage($page){
		$this->page = $page;
		return true;
	}
	
	public function changePage($page){
		//VALIDATIE!
		if ($this->retrievePage($page['pageName'])){
			$this->db->UpdateRows("pages", $page, array('pageName' => $page['pageName']));
			if ($this->db->Error()) $this->db->Kill();
			$this->retrievePage($page['pageName']);
		}
	}
	
}

?>