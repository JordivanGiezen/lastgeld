<?php

	///////////////////
	// Initialisatie //
	///////////////////

	// Load configfile
	require_once('config/config.php'); 
	
	// Load MySQL class
	require_once('classes/mysql.class.php');
	
	//Load global functions (optional)
	if (is_file('include/functies.php')){
		require_once('include/functions.php');
	}
	
	//Load the global controller
	require_once('controllers/controller.php');

	//load the model
	require_once('models/model.php');
	
	//load the global view
	require_once('views/view.php');
	
	//load the template
	require_once('templates/template.php');
	
	// Force HTTPS?
	if (FORCE_HTTPS AND (!isset($_SERVER['HTTPS']) OR $_SERVER['HTTPS'] != "on")) {
		$url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		header("Location: $url");
		exit;
	}

	// Connect to the database and kill when it errors 
	$db = new MySQL(true, $mysql['database'], $mysql['server'], $mysql['user'], $mysql['password']);
	if ($db->Error()) $db->Kill();
	
	////////////////////////////////
	// Select the right MVC parts //
	////////////////////////////////
	
	// Parse the page and it's arguments
	if(!isset($_GET['page'])) {
		$_GET['page'] = "index";
		$page = "index";
	} else {
		$page = $_GET['page'];
	}
	
	foreach($_GET as $key => $value){
		$args[$key] = $value;
	}
	
	foreach($_POST as $key => $value){
		if (!isset($args[$key])){
			$args[$key] = $value;
		} else {
			exit('Fatal error: More than one variable with the same name.'); //Dit zou echt heel stom zijn.
		}
	}

	//Only one database so one global model will suffice. Changes have to be made when using more or bigger databases
	$model = new Model($db);
	
	
	
	// Load the extended controller if there is one available
	if (is_file('controllers/'.$page.'.php')){
		require_once('controllers/'.$page.'.php');
		$controller = new ExtendedController($model, $args);
	} else {
		$controller = new Controller($model, $args);
	}
	
	
	//Load the extended view if there is one available
	if (is_file('views/'.$page.'.php')){
		require_once('views/'.$page.'.php');
		$view = new ExtendedView($model);
	} else {
		$view = new View($model);
	}
	
	
	////////////////////////////
	// Get the right template //
	////////////////////////////
	
	if (is_file('template/'.$page.'.php')){
		require_once('templates/'.$page.'.php');
		$tpl = new ExtendedTemplate($view);
	} else {
		$tpl = new Template($view);
	}
	
	
	echo $tpl->output();
		
?>