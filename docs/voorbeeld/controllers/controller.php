<?php

class Controller {
	
	private $args = "";
	private $model = "";
	
	public function __construct($model, $args){
		$this->args = $args;
		$this->model = $model;
		$this->getPageReady();

	}

	private function getPageReady(){
		$page = $this->args['page'];
		$this->model->retrievePage($page);
		
		$props = $this->model->getPage();
		
		if ($props['pageMenu'] == true){
			$this->model->retrieveMenu();
		}
		
		if ($props['pageHeader'] == true){
			$this->model->retrieveHeader();
		}
		
		if($props['pageAgenda'] == true){
			$this->model->retrieveAgenda();
		}
	}

}

?>