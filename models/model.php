<?php

class Model {
	
	private $db = "";
	private $controller = "";
	private $searchResult;
	private $searchQuery;
	
	public function __construct($db, $controller){
		$this->db = $db;	//No setter since models shouldn't be used on more than one database and this one should also not be used without a db.	
		$this->controller = $controller;
		
		//$this->page = $this->controller->getPage(); //not used in this project
		$this->processedArgs = $this->controller->getProArgs(); 
		
		//$this->searchPage(); //not used in this project
		$this->processAll();
	}

	private function processAll(){
		$process = $this->processedArgs;
		if(is_array($process)){
			foreach($process as $key => $value){
				$tempResult = $this->$key($value);
				$this->searchQuery[$key] = $tempResult['query'];
				$this->searchResult[$key] = $tempResult['result'];
			}
		}
	}
	
	public function getSearchResult(){
		return $this->searchResult;
	}
	
	public function getSearchQuery(){
		return $this->searchQuery;
	}
		
	/**
	* Function that escapes the text in an array
	*/
	public function escapeArray($data){
		foreach($data as $key => $value){
			$data[$key] = "'".$this->db->SQLFix($value)."'";
		}
	}
			
	/**	Function that retrieves the information based on a genealogical search request
	 *  
	 *  @param array $data. Array with 3 to 10 values: begin, end, [lastNameStandard, soundex(lastNameStandard), lastNameCaptain, firstNameStandard, soundex(firstNameStandard), firstNameCaptain, portAreas.region, portAreas.country]
	 *  lastNameCaptain cannot be combined with lastNameStandard and soundex(lastNameStandard). firstNameCaptain cannot be combined with firstNameStandard and soundex(firstNameStandard).
	*/
	public function graphSearch($data){
		$full = false;
		if(isset($data['showall'])){
			$full = true;
			unset($data['showall']);
		}
		//We're building a graph. We know that xAxis, yAxis, begindate, enddate and graphname are set but these have to be modified or dissapear before we build the query.
		$graphTitle = $data['graphname'];
		unset($data['graphname']);
		$group = $data['xAxis'];
		unset($data['xAxis']);
		$interval = $data['yAxis'];
		unset($data['yAxis']);
		$absoluteBegin = $data['begin'];
		unset($data['begin']);
		$absoluteEnd = $data['end'];
		unset($data['end']);
		//fit all the data in the right format and escape the values.
		foreach($data as $key => $value){
			$querydata[$key] = array('column' => $key,
								 'operator' => '=',
								 'value' => '\''.$this->db->SQLFix($value).'\''
								);
		}
		
		//modify the operator to fit wildcards
		$wildcards = array('lastNameCaptain', 'firstNameCaptain', 'ports.portName', 'ports.portCode');
		foreach($wildcards as $value){
			if (isset($querydata[$value])){
				$querydata[$value]['operator'] = "LIKE";
			}
		}
		
		$travelInformation = array('tax' => 'taxGuilders', 
									'tons' => 'tons',
									'loads' => 'numberOfCargoes'
									);
		foreach($travelInformation as $key => $value){							
			if (isset($querydata[$value])){
				if ($querydata[$key . 'Operator']['value'] == "'Between'"){
					$querydata[$value] = array('column' => 'lastgeld.'.$value,
										 'operator' => 'BETWEEN',
										 'value' => $querydata[$value]['value'] ." AND ".$querydata[$key . 'Max']['value']
										);
					unset($querydata[$key . 'Max']);
				} else {
					$querydata[$value]['operator'] = substr($querydata[$key . 'Operator']['value'], 1, -1);
				}
				unset($querydata[$key . 'Operator']);
			}		
		}
		
		$columns = array('*');
		
		if ($group == 'taxGuilders'){
			$columns[] = 'SUM(taxGuilders)';
		} else if ($group == 'tons'){
			$columns[] = 'SUM(tons)';
		} else if ($group == 'numberOfCargoes'){
			$columns[] = 'SUM(numberOfCargoes)';
		} else if ($group == 'idLastgeld'){
			$columns[] = 'COUNT('.$group.')';
			$group = null;
		} else {
			$columns[] = 'COUNT('.$group.')';
		}
				
		//loop so we can actually query over the interval that is asked.
		//Queries are slow...
		$begin = $absoluteBegin;
		do {
			$end = $this->newEndDate($begin, $absoluteEnd, $interval);
			
			$querydata['lastgeld.date'] = array('column' => 'lastgeld.date',
									 'operator' => 'BETWEEN',
									 'value' => "'".$begin ."' AND '".$end."'"
									);
				
			$temp['query'][$begin] = $this->db->BuildSQLSelect('lastgeld', $querydata, $columns, null, true, null, " JOIN ports ON ports.portCode = lastgeld.portCode JOIN portAreas ON lastgeld.portCodeNumber BETWEEN portAreas.startCode AND portAreas.endCode", $group);
			if (!$this->db->Query($temp['query'][$begin])) echo $this->db->Kill();
			$temp['result'][$begin] = $this->db->RecordsArray(MYSQLI_ASSOC, true);
			$begin = $end;
		} while($end != $absoluteEnd);
		
		if (is_null($group)){
			$querydata['lastgeld.date'] = array('column' => 'lastgeld.date',
												 'operator' => 'BETWEEN',
												 'value' => "'".$absoluteBegin ."' AND '".$absoluteEnd."'"
												);
			$temp['query']['Full'] = $this->db->BuildSQLSelect('lastgeld', $querydata, null, null, true, null, " JOIN ports ON ports.portCode = lastgeld.portCode JOIN portAreas ON lastgeld.portCodeNumber BETWEEN portAreas.startCode AND portAreas.endCode");
			//echo $temp['query']['Full']."<br />";
			if (!$this->db->Query($temp['query']['Full'])) echo $this->db->Kill();
			$temp['result']['Full'] = $this->db->RecordsArray(MYSQLI_ASSOC, true);
		}
		return $temp;
	}
	
	public function advancedSearch($data){
		
		//TODO Fix this so that queries can be modified to the max ;) Also in the controller
		
		//escape the data and parse it into a new array so we can use it for the BuildSQLWhereClause();
		foreach($data as $key => $value){
			$querydata[$key] = array('column' => $key,
								 'operator' => '=',
								 'value' => '\''.$this->db->SQLFix($value).'\''
								);
		}
		
		//modify the operator and data to fit a proper between for the date.
		$querydata['lastgeld.date'] = array('column' => 'lastgeld.date',
								 'operator' => 'BETWEEN',
								 'value' => $querydata['begin']['value'] ." AND ".$querydata['end']['value']
								);
		unset($querydata['begin']);
		unset($querydata['end']);
		
		//modify the operator to fit wildcards
		$wildcards = array('lastNameCaptain', 'firstNameCaptain', 'ports.portName', 'ports.portCode');
		foreach($wildcards as $value){
			if (isset($querydata[$value])){
				$querydata[$value]['operator'] = "LIKE";
			}
		}
		
		$travelInformation = array('tax' => 'taxGuilders', 
									'tons' => 'tons',
									'loads' => 'numberOfCargoes'
									);
		foreach($travelInformation as $key => $value){							
			if (isset($querydata[$value])){
				if ($querydata[$key . 'Operator']['value'] == "'Between'"){
					$querydata[$value] = array('column' => 'lastgeld.'.$value,
										 'operator' => 'BETWEEN',
										 'value' => $querydata[$value]['value'] ." AND ".$querydata[$key . 'Max']['value']
										);
					unset($querydata[$key . 'Max']);
				} else {
					$querydata[$value]['operator'] = substr($querydata[$key . 'Operator']['value'], 1, -1);
				}
				unset($querydata[$key . 'Operator']);
			}		
		}
		
		$temp['query'] = $this->db->BuildSQLSelect('lastgeld', $querydata, null, null, true, null, " JOIN ports ON ports.portCode = lastgeld.portCode JOIN portAreas ON lastgeld.portCodeNumber BETWEEN portAreas.startCode AND portAreas.endCode");

		if (!$this->db->Query($temp['query'])) echo $this->db->Kill();
		$temp['result'] = $this->db->RecordsArray(MYSQLI_ASSOC, true);
		
		return $temp;
	}
	
	public function genealogicalSearch($data){
		
	
		//escape the data and parse it into a new array so we can use it for the BuildSQLWhereClause();
		foreach($data as $key => $value){
			$querydata[$key] = array('column' => $key,
								 'operator' => '=',
								 'value' => '\''.$this->db->SQLFix($value).'\''
								);
		}
		
		if (isset($querydata['lastNameCaptain'])){
			$querydata['lastNameCaptain']['operator'] = "LIKE";
		}
		if (isset($querydata['firstNameCaptain'])){
			$querydata['firstNameCaptain']['operator'] = "LIKE";
		}
		//format the data in such a way we can use the 2 dates in a between clause
		$querydata['lastgeld.date'] = array('column' => 'lastgeld.date',
								 'operator' => 'BETWEEN',
								 'value' => $querydata['begin']['value'] ." AND ".$querydata['end']['value']
								);
		unset($querydata['begin']);
		unset($querydata['end']);
		
		
		
		$temp['query'] = $this->db->BuildSQLSelect('lastgeld', $querydata);
		if (!$this->db->Query($temp['query'])) echo $this->db->Kill();
		$temp['result'] = $this->db->RecordsArray(MYSQLI_ASSOC, true);
		return $temp;
	}
	
	public function regionalSearch($data){
				
		//escape the data and parse it into a new array so we can use it for the BuildSQLWhereClause();
		foreach($data as $key => $value){
			$querydata[$key] = array('column' => $key,
								 'operator' => '=',
								 'value' => '\''.$this->db->SQLFix($value).'\''
								);
		}
		
		if (isset($querydata['ports.portName'])){
			$querydata['ports.portName']['operator'] = "LIKE";
		}
		if (isset($querydata['ports.portCode'])){
			$querydata['ports.portCode']['operator'] = "LIKE";
		}
		//ugly as [metafoor]
		
		$table = "ports";
		$join = " JOIN portAreas ON ports.areaCode = portAreas.areaCode JOIN lastgeld ON ports.portCode = lastgeld.portCode";
		if (isset($querydata['portAreas.region'])){
			$table = "portAreas";
			$join = " JOIN ports ON portAreas.areaCode = ports.areaCode JOIN lastgeld ON lastgeld.portCodeNumber BETWEEN portAreas.startCode AND portAreas.endCode";
		}
		
		$temp['query'] = $this->db->BuildSQLSelect($table, $querydata, null, null, true, null, $join);
		if (!$this->result = $this->db->Query($temp['query'])) echo $this->db->Kill();
		
		$temp['result'] = $this->db->RecordsArray(MYSQLI_ASSOC, true);
		return $temp;
		
	}
	
	public function getRegionalSearch(){
		return array('result' => $this->result, 'query' => $this->query);
	}	
	
	private function retrieveArguments(){
		return $this->controller->getArguments();
	}
	
	public function getArguments(){
		return self::retrieveArguments();
	}
	
	private function newEndDate($begin, $absoluteEnd, $interval){
		switch($interval){
			case 'Years':
				$period = 'P1Y';
				break;
			case 'Half-years':
				$period = 'P6M';
				break;
			case 'Quarters':
				$period = 'P3M';
				break;
			case 'Months':
				$period = 'P1M';
				break;
		}
		$date = new DateTime($begin);
		$date->add(new DateInterval($period));
		$end = $date->format('Y-m-d');
		$datetime1 = new DateTime($end);
		$datetime2 = new DateTime($absoluteEnd);
		$endDateDifference = $datetime1->diff($datetime2);
		
		//no need for something special if we're already at the end.
		if ($endDateDifference->format('%R') != "+"){
			return $absoluteEnd;
		}
		
		//The years and months are easy to figure out.
		if ($interval == 'Years'){
			return $date->format('Y-01-01');
		} else if ($interval == 'Months'){
			return $date->format('Y-m-01');
		}
		
		//half years and quarters need the months to be floored to the nearest relevant value.
		if ($interval == 'Half-years'){
			$months = $date->format('m');
			if ($months >= 7){
				$months = 7;
			} else {
				$months = 1;
			}
			return $date->format('Y-'.$months.'-01');
		} else {
			$months = $date->format('m');
			if ($months >= 10){
				$months = 10;
			} else if ($months >= 7){
				$months = 7;
			} else if ($months >= 4){
				$months = 4;
			} else {
				$months = 1;
			}
			return $date->format('Y-'.$months.'-01');
		}
		
		return $end;
	}
	
}
?>
