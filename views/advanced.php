<?php

class ExtendedView extends View {

	public function __construct($model){
		parent::__construct($model);
	}
	
	public function getAdvancedView(){
		return $this->parseSearchResult();
	}
	
	public function getSearchQueryAdvanced(){
		return $this->searchQuery['advancedSearch'];
	}
	
	public function getParseForMaps(){
		return $this->parseSearchResultForMaps();
	}
	
	protected function parseSearchResult(){ //uitbreiden / aanpassen voor bijv. statistieken?
		//retrieve data from the model
		$data = $this->searchResult['advancedSearch'];
		$dataInRows = array();

		//$columns['ID'] = 'idLastgeld';
		$columns['Date'] = 'date';
		$columns['First name'] = 'firstNameCaptain';
		$columns['Last name'] = 'lastNameCaptain';
		$columns['Departure port'] = 'departurePort';
		$columns['Cargoes'] = 'numberOfCargoes';
		$columns['Tons'] = 'tons';
		$columns['Tax in Guilders'] = 'taxGuilders';
		$columns['Entry'] = 'fileName';

		if ($data == false){ 
			//empty result or error, we should give a nice error msg here
		} else {
			foreach($data as $row){
				$id = $row['idLastgeld'];
				//keys renamen
				foreach($columns as $key => $value){
					if(isset($row[$value])){
						$newRow[$key] = $row[$value];
					}
					unset($row[$value]);
				}
				$dataInRows[$id] = $newRow;
			}
		}
		return $dataInRows;
	}
	
	protected function parseSearchResultForMaps(){
	
		$columns['portName'] = 'portName';
		$columns['Country'] = 'countryNow';
		$columns['cargoes'] = 'numberOfCargoes';
		$columns['tons'] = 'tons';
		$columns['tax'] = 'taxGuilders';
		$columns['lat'] = 'lat';
		$columns['lng'] = 'lng';
		
		$data = $this->searchResult['advancedSearch'];
		
		$portData = array();
		
		if ($data == false){ 
			//empty result or error, we should give a nice error msg here
		} else {
			foreach($data as $row){
				$portname = $row['portName'];
				if (isset($portData[$portname])){
					$portData[$portname]['total'] += 1;
					$portData[$portname]['cargoes'] += $row['numberOfCargoes'];
					$portData[$portname]['tons'] += $row['tons'];
					$portData[$portname]['tax'] += $row['taxGuilders'];
				} else {
					$portData[$portname]['lat'] = $row['lat'];
					$portData[$portname]['lng'] = $row['lng'];
					$portData[$portname]['total'] = 1;
					$portData[$portname]['cargoes'] = $row['numberOfCargoes'];
					$portData[$portname]['tons'] = $row['tons'];
					$portData[$portname]['tax'] = $row['taxGuilders'];
				}				
			}
			$output = "";
			foreach($portData as $port => $portDetails){
				$output .= "{portName:\"".$port."\", lat:\"".$portDetails['lat']."\",lng:\"".$portDetails['lng']."\",total:\"".$portDetails['total']."\",cargoes:\"".$portDetails['cargoes']."\",tons:\"".$portDetails['tons']."\",tax:\"".$portDetails['tax']."\"},";
			}
			$output = substr($output, 0, -1);
		}
		
		return $output;
	}
	
	protected function parseSearchResultForDownload(){
		//retrieve data from the model
		$data = $this->searchResult['advancedSearch'];
		$dataInRows = array();

		if ($data == false){ 
			//empty result or error, we should give a nice error msg here
		} else {
			array_unshift($data, array_keys(current($data)));
		}
		return $data;
	}
	
	public function downloadCSV(){
		$input = self::parseSearchResultForDownload();
		return self::arrayToCsv($input);
	}
	
}		

?>
