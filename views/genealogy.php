<?php

class ExtendedView extends View {

	public function __construct($model){
		parent::__construct($model);
	}
	
	public function getGenealogyView(){
		return $this->parseSearchResult();
	}
	
	public function getSearchQueryGenealogy(){
		return $this->searchQuery['genealogicalSearch'];
	}
	
	protected function parseSearchResult(){ //uitbreiden / aanpassen voor bijv. statistieken?
		//retrieve data from the model
		$data = $this->searchResult['genealogicalSearch'];
		$dataInRows = array();

		//$columns['ID'] = 'idLastgeld';
		$columns['First name'] = 'firstNameCaptain';
		$columns['Last name'] = 'lastNameCaptain';
		$columns['Date'] = 'date';
		$columns['Departure port'] = 'departurePort';
		$columns['Entry'] = 'fileName';

		if ($data == false){ 
			//empty result or error, we should give a nice error msg here
		} else {
			foreach($data as $row){
				$id = $row['idLastgeld'];
				//keys renamen
				foreach($columns as $key => $value){
					if(isset($row[$value])){
						$newRow[$key] = $row[$value];
					}
					unset($row[$value]);
				}
				$dataInRows[$id] = $newRow;
			}
		}
		return $dataInRows;
	}
	
	protected function parseSearchResultForDownload(){
		//retrieve data from the model
		$data = $this->searchResult['genealogicalSearch'];
		$dataInRows = array();

		if ($data == false){ 
			//empty result or error, we should give a nice error msg here
		} else {
			array_unshift($data, array_keys(current($data)));
		}
		return $data;
	}
	
	public function downloadCSV(){
		$input = self::parseSearchResultForDownload();
		return self::arrayToCsv($input);
	}
	
}		

?>
