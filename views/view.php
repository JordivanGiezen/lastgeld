<?php

class View {
	
	protected $model = "";
	protected $query = "";
	protected $searchResult = "";
	protected $searchQuery = "";

	public function __construct($model){
		$this->model = $model;
		
		$this->setSearchResult();
		
		$this->setSearchQuery();
		
	}
	
	private function setSearchResult(){
		$this->searchResult = $this->model->getSearchResult(); //double array
	}
	
	protected function getSearchResult(){
		return $this->searchResult;
	}
	
	private function setSearchQuery(){
		$this->searchQuery = $this->model->getSearchQuery(); //double array
	}
	
	protected function getSearchQuery(){
		return $this->searchQuery;
	}
	
	private function retrieveArguments(){ //not setting it, just passing through
		return $this->model->getArguments();
	}
	
	public function getArguments(){
		return self::retrieveArguments();
	}
	
	public function getDownloadArguments(){
		$download = "&action=download";
		foreach(self::retrieveArguments() as $key => $value){
			if ($key == "page"){
				$start = $value."/";
			} else {
				if (!empty($value)){
					$download .= "&".$key."=".$value;
				}
			}
		}
		return $start.$download;
	}
	
	public function noSearch(){
		if (empty($this->searchResult)){
			return true;
		}
		return false;
	}
	
	public function isDownload(){
		$args = self::getArguments();
		if (isset($args['action']) && $args['action'] == "download"){
			return true;
		}
		return false;
	}
	

	public function arrayToCsv($input, $delimiter = ';', $enclosure = '"', $encloseAll = false, $nullToMysqlNull = false ) {
		foreach($input as $fields){
			$delimiter_esc = preg_quote($delimiter, '/');
			$enclosure_esc = preg_quote($enclosure, '/');

			$output = array();
			foreach ( $fields as $field ) {
				if ($field === null && $nullToMysqlNull) {
					$output[] = 'NULL';
					continue;
				}

				// Enclose fields containing $delimiter, $enclosure or whitespace
				if ( $encloseAll || preg_match( "/(?:${delimiter_esc}|${enclosure_esc}|\s)/", $field ) ) {
					$output[] = $enclosure . str_replace($enclosure, $enclosure . $enclosure, $field) . $enclosure;
				}
				else {
					$output[] = $field;
				}
			}
			$line[] = implode( $delimiter, $output );
		}
		return implode( "\r\n", $line );
	}
	
}

?>