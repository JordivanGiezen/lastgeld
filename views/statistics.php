<?php

class ExtendedView extends View {

	public function __construct($model){
		parent::__construct($model);
	}
	
	public function getGraphView(){
		return $this->parseSearchResult();
	}
	
	public function getSearchQueryAdvanced(){
		return $this->searchQuery['graphSearch'];
	}
	
	public function getParseForGraph(){
		return $this->parseSearchResultForGraph();
	}
	
	public function getNrRecords(){
		$total = 0;
		foreach($this->searchResult['graphSearch'] as $key => $value){
			if (is_array($value)){
				$total += count($value);
			}
		}
		return $total;
	}
	
	protected function parseSearchResult(){ //uitbreiden / aanpassen voor bijv. statistieken?
		//retrieve data from the model
		if (isset($this->searchResult['graphSearch']['Full'])){
			$data = $this->searchResult['graphSearch']['Full'];
			$dataInRows = array();

			//$columns['ID'] = 'idLastgeld';
			$columns['Date'] = 'date';
			$columns['First name'] = 'firstNameCaptain';
			$columns['Last name'] = 'lastNameCaptain';
			$columns['Departure port'] = 'departurePort';
			$columns['Cargoes'] = 'numberOfCargoes';
			$columns['Tons'] = 'tons';
			$columns['Tax in Guilders'] = 'taxGuilders';
			$columns['Entry'] = 'fileName';

			if ($data == false){ 
				//empty result or error, we should give a nice error msg here
			} else {
				foreach($data as $row){
					$id = $row['idLastgeld'];
					//keys renamen
					foreach($columns as $key => $value){
						if(isset($row[$value])){
							$newRow[$key] = $row[$value];
						}
						unset($row[$value]);
					}
					$dataInRows[$id] = $newRow;
				}
			}
			return $dataInRows;
		}
		return null;
	}
	
	protected function parseSearchResultForGraph(){
		$output = "";
		$data = $this->searchResult['graphSearch'];
		if (isset($data['Full'])){
			unset($data['Full']);
		}
		
		foreach($data as $key => $value){
			if (is_array($value)){ //there are results!
				$graph[$key] = array();
				foreach($value as $iteration => $row){
					foreach($row as $columns => $values){
						if (preg_match("/count\((.*)\)/i", $columns, $match)){
							$temp = $match[1];
							$title = $match[1];
						} else if(preg_match("/sum\((.*)\)/i", $columns, $match)){
							$temp = $match[1];
						}
						if (isset($temp)){
							@$graph[$key][$temp] += $values;
						}
						unset($temp);
					}
					
				}
				if (isset($title)){
					$graph[$key]['Travels'] = $graph[$key][$title];
					$graph[$key][$title] = $iteration+1; //iteratie begint bij 0.
				}
				if (isset($title) && $title == "idLastgeld"){
					unset($graph[$key][$title]);
				}
			}
		}
		
		$labels = "['Period'";		
		foreach($graph as $title => $rows){
			foreach($rows as $key => $value){
				$labels .= ",'".$key."'";
			}
			break;
		}
		$labels .= "]";
		
		$output = "";
		foreach($graph as $title => $rows){
			
			$output .= ",['".$title."'";
			foreach($rows as $key => $value){
				$output .= ", ".$value;
			}
			$output .= "]";
		}
		
		$output = $labels.$output;
		
		return $output;
	}
	
	protected function parseSearchResultForDownload(){
		//retrieve data from the model
		$data = $this->searchResult['graphSearch']['Full'];
		$dataInRows = array();

		if ($data == false){ 
			//empty result or error, we should give a nice error msg here
		} else {
			array_unshift($data, array_keys(current($data)));
		}
		return $data;
	}
	
	public function downloadCSV(){
		$input = self::parseSearchResultForDownload();
		return self::arrayToCsv($input);
	}
	
}		

?>
