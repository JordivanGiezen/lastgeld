<?php

	//CONFIG FILE
	
	$mysql = array(
				'user' => '', // Input a mysql user
				'password' => '', // Input the user's password, if any
				'server' => '', // Where can the mysql server be found
				'database' => '' // The name of the databases
				);

	//REWRITE? 
	define('URL_REWRITE', false); //Looks fancy when it works

	// Force HTTPS?
	define('FORCE_HTTPS', false); //Should be true. 

	
	//SETTINGS
	
	date_default_timezone_set('Europe/Amsterdam'); //Dutch website
	ini_set('memory_limit', 134217728); //Just to be sure
	ini_set('default_charset', 'UTF-8'); //standard in PHP > 5.6.0	
?>