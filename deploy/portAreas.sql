-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 03, 2015 at 11:58 PM
-- Server version: 5.5.38-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbdwt`
--

-- --------------------------------------------------------

--
-- Table structure for table `portAreas`
--

CREATE TABLE IF NOT EXISTS `portAreas` (
  `area` varchar(40) NOT NULL,
  `countriesNow` varchar(20) DEFAULT NULL,
  `areaCode` decimal(5,0) NOT NULL DEFAULT '0',
  `startCode` decimal(6,0) DEFAULT NULL,
  `endCode` decimal(6,0) DEFAULT NULL,
  PRIMARY KEY (`areaCode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `portAreas`
--

INSERT INTO `portAreas` (`area`, `countriesNow`, `areaCode`, `startCode`, `endCode`) VALUES
('Denmark', 'Denmark', 1, 1, 70),
('Holstein', 'Germany', 2, 71, 90),
('Iceland', 'Iceland', 3, 91, 91),
('The Faroe Islands', 'Faeroe Islands', 4, 92, 92),
('Greenland and Davis Strait', 'Greenland', 5, 93, 98),
('Other northern ports in Danish Kingdom', 'Denmark', 6, 99, 100),
('Norway', 'Norway', 7, 101, 200),
('Sweden and Finland', NULL, 8, 201, 300),
('Russia around St. Petersburg', 'Russia', 9, 301, 340),
('Esthonia', 'Estonia', 10, 341, 350),
('Livland', 'Russia', 11, 351, 370),
('Kurland', 'Russia', 12, 371, 400),
('South Baltic: East Prussia to Lubeck', 'Poland', 13, 401, 490),
('The Baltic without specification', 'Poland', 14, 491, 500),
('The Netherlands', 'Netherlands', 15, 501, 600),
('Scotland', 'United Kingdom', 16, 601, 630),
('England and Wales', 'United Kingdom', 17, 631, 678),
('Ireland', 'Ireland', 18, 689, 689),
('Hamburg, Bremen, Kleine Oost', 'Germany', 19, 701, 770),
('The Austrian Netherlands', 'Belgium', 20, 771, 780),
('North Sea withou specification', 'Germany', 21, 789, 789),
('Russian Arctic ports (Archangel)', 'Russia', 22, 791, 800),
('France - Atlantic Coast', 'France', 23, 801, 810),
('Portugal', 'Portugal', 24, 811, 820),
('Spain', 'Spain', 25, 821, 830),
('France - Mediterranean Coast', 'France', 26, 831, 840),
('Italy', 'Italy', 27, 841, 860),
('Ports at the Black Sea', 'Ukraine', 28, 861, 870),
('Other European and Asian Ports in Medit.', 'Greece', 29, 871, 880),
('African Mediterranean Ports', NULL, 30, 881, 890),
('Mediterranean - unspecified', 'Turkey', 31, 891, 900),
('Africa outside the Mediterranean', NULL, 32, 901, 910),
('The Far East', NULL, 33, 911, 920),
('Latin America', NULL, 34, 921, 930),
('West-Indies', NULL, 35, 931, 940),
('North America', NULL, 36, 941, 990),
('Unindentifiable Ports', NULL, 37, 999, 999);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
