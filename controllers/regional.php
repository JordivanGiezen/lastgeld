<?php

class ExtendedController extends Controller {
	
	public function __construct($args){
		parent::__construct($args);
	}

	protected function processPost($data){
		//4 possible values: surname, firstName, date, portOfOrigin
		//idea is to tell these values to the model in such a way that it can build / extend a query with the results (and validate the data)
		if (count($data) > 1){
			$queryArray = null;
					
			/*
			 * Adding region or modern country to the array
			 */
			 
			if (!empty($data['port'])){
				$queryArray['ports.portName'] = $data['port'];
			}
			
			if (!empty($data['region'])){
				$queryArray['portAreas.area'] = $data['region'];
			}
			
			if (!empty($data['country'])){
				$queryArray['ports.countryNow'] = $data['country'];
			}
			
			if (!empty($data['soundcode'])){
				$queryArray['ports.portCode'] = $data['soundcode'];
			}
			
			if (is_null($queryArray)){
				//vies ranzig bah jakkie
				
				//FOUTMELDING
				//FOUTMELDING
				//FOUTMELDING
				//FOUTMELDING
				die('"Foutmelding"');
			}
			
			$this->processedArgs['regionalSearch'] = $queryArray;
		}
	}
}
?>
