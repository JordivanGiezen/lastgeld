<?php

/**
 * The controller that makes sure all the data arrives at the proper places and will be processed
 */
class Controller {

	/**
	 * Will contain any arguments that have been passed to the controller upon creation
	 * @var string Arguments passed to controller upon construction
     */
	protected $args = "";
	/**
	 * Will contain the link to the database that is to be used for all the actions that this controller will do.
	 *
	 * @var string Link to database
     */
	protected $model = "";
	
	protected $processedArgs;

	/**
	 * Constructor of the controller, which saves the link to the database and any arguments.
	 *
	 * @param $model The database to use
	 * @param $args Any arguments you might have
     */
	public function __construct($args){
		$this->args = $args;
		$this->processPost($this->args);
	}

	protected function processPost($data){
		//Hier hadden we iets met de pagina kunnen doen. Nu niet :D
		$this->processedArgs = array();
	}
	
	public function getProArgs(){
		return $this->processedArgs;
	}
	
	public function getArguments(){
		return $this->args;
	}

}
?>
