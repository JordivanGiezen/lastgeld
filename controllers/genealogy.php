<?php

/**
 * Class that is used when someone is on the genealogy page.
 */
class ExtendedController extends Controller {

	/**
	 * Constructor that uses the super (@see Controller::__construct($model, $args)) and checks if there is data to
	 * process. If this is the case, it will process it.
	 *
	 * @param $model The database to use
	 * @param $args Any arguments you might have
     */
	public function __construct($args){
		parent::__construct($args);
	}
	
	private function magicSwap(&$x, &$y){
		$tmp=$x;
		$x=$y;
		$y=$tmp;
	}

	/**
	 * Processes any data that have been posted in the form.
	 *
	 * @param $data The data that was submitted using the form
     */
	protected function processPost($data){
		if (count($data) > 1){
			$queryArray = null;
			
			/*
			 * Adding surname and/or first name to the array if it is submitted
			 * 
			 * Searching without wildcard is directed to the non standardized database column, with wildcard we are going to use soundex to compare the submitted value to the standardized column
			 */
			 
			if (!empty($data['lastname'])){
				if (isset($data['lastname_wc'])){
					$queryArray['SOUNDEX(lastgeld.lastNameStandard)'] = soundex($data['lastname']);
				} else {
					$queryArray['lastNameCaptain'] = $data['lastname'];
				}
			}
			
			if (!empty($data['firstname'])){
				if (isset($data['firstname_wc'])){
					$queryArray['SOUNDEX(lastgeld.firstNameStandard)'] = soundex($data['firstname']);
				} else {
					$queryArray['firstNameCaptain'] = $data['firstname'];
				}
			}
			
			/*
			 * Resetting the date to the first known date in the database when it's empty or doesn't match the regex for a date. If it's a valid date to start with it remains untouched ofcourse.
			 * 
			 * Resetting the dates results in an easier query for the model since we're now sure it is included.
			 */
			 
			if (!isset($data['begin']) || !preg_match("/(\d\d)-(\d\d)-(\d\d\d\d)/", $data['begin'], $date)){ //serieus, waarom zou je hieraan gaan zitten...
				$data['begin'] = '1744-04-01';
			}
			$queryArray['begin'] = $date[3]."-".$date[2]."-".$date[1];
			
			if (!isset($data['end']) || !preg_match("/(\d\d)-(\d\d)-(\d\d\d\d)/", $data['end'], $date)){ //serieus, waarom zou je hieraan gaan zitten...
				$data['end'] = '1748-12-31';
			}
			$queryArray['end'] = $date[3]."-".$date[2]."-".$date[1];
			
			
			//switch the dates if they were in a valid format but the wrong way round.
			$datetime1 = new DateTime($queryArray['begin']);
			$datetime2 = new DateTime($queryArray['end']);
			$interval = $datetime1->diff($datetime2);
			
			if ($interval->format('%a') < 0){
				$this->magicSwap($queryArray['begin'], $queryArray['end']);
			}
			
			if (count($queryArray) == 2){
				//vies ranzig bah jakkie
				
				//FOUTMELDING
				//FOUTMELDING
				//FOUTMELDING
				//FOUTMELDING
				die('"Foutmelding"');
			}
			
			$this->processedArgs['genealogicalSearch'] = $queryArray;
		}
	}
}
?>
